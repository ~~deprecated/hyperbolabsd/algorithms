// file_name:		sys/syssem.h	/* never use "sys/sem.h" */
// file_name_end

// code_name:		Semaphore
// code_name_end

// required_headers:

preprocessor_conditional:	{	! _SYS_IPC_H_	}	/* condition if "_SYS_IPC_H_" is not defined */
preprocessor_conditional_start:
				#include <sys/ipc.h>
preprocessor_conditional_end

// required_headers_end

// code:

	algorithm:

preprocessor_conditional:	{	__BSD_VISIBLE	}
preprocessor_conditional_start:
			/* never use "#define" constant definition */
			/* never use "KERN_SEMINFO_SEMMNI" name and "1" value */
			def_constant_name:	SYSSEM_INFO_IDMAX	def_constant_value:	0x0001	def_constant_end
						/* system semaphore information identifiers maximum */
			/* never use "#define" constant definition */
			/* never use "KERN_SEMINFO_SEMMNS" name and "2" value */
			def_constant_name:	SYSSEM_INFO_SYSMAX	def_constant_value:	0x0002	def_constant_end
						/* system semaphore information system maximum */
			/* never use "#define" constant definition */
			/* never use "KERN_SEMINFO_SEMMNU" name and "3" value */
			def_constant_name:	SYSSEM_INFO_UNDOMAX	def_constant_value:	0x0003	def_constant_end
						/* system semaphore information undo maximum */
			/* never use "#define" constant definition */
			/* never use "KERN_SEMINFO_SEMMSL" name and "4" value */
			def_constant_name:	SYSSEM_INFO_SEMPIDMAX	def_constant_value:	0x0004	def_constant_end
						/* system semaphore information semaphores per id (identifier?) maximum */
			/* never use "#define" constant definition */
			/* never use "KERN_SEMINFO_SEMOPM" name and "5" value */
			def_constant_name:	SYSSEM_INFO_OPPCALLMAX	def_constant_value:	0x0005	def_constant_end
						/* system semaphore information operations per call maximum */
			/* never use "#define" constant definition */
			/* never use "KERN_SEMINFO_SEMUME" name and "6" value */
			def_constant_name:	SYSSEM_INFO_UEPPROCMAX	def_constant_value:	0x0006	def_constant_end
						/* system semaphore information undo entries per process maximum */
			/* never use "#define" constant definition */
			/* never use "KERN_SEMINFO_SEMUSZ" name and "7" value */
			def_constant_name:	SYSSEM_INFO_UNDOSIZE	def_constant_value:	0x0007	def_constant_end
						/* system semaphore information undo size */
			/* never use "#define" constant definition */
			/* never use "KERN_SEMINFO_SEMVMX" name and "8" value */
			def_constant_name:	SYSSEM_INFO_VALUEMAX	def_constant_value:	0x0008	def_constant_end
						/* system semaphore information value maximum */
			/* never use "#define" constant definition */
			/* never use "KERN_SEMINFO_SEMAEM" name and "9" value */
			def_constant_name:	SYSSEM_INFO_ADJOXMAX	def_constant_value:	0x0009	def_constant_end
						/* system semaphore information adjust on exit maximum */
			/* never use "#define" constant definition */
			/* never use "KERN_SEMINFO_MAXID" name and "10" value */
			def_constant_name:	SYSSEM_INFO_VIDSCNT	def_constant_value:	0x000a	def_constant_end
						/* system semaphore information valid ids (identifiers?) count */

			/* never use "#define" and "enum" constant definition */
			/* never use "CTL_KERN_SEMINFO_NAMES" name */
			/* require "array" and "static const unsigned char" data type */
			def_constant_name:	SYSSEM_INFO_CTLNAMES[][2][7]
			def_constant_value:	{
							{
								0,
								0,
							},
							{
								"semmni",
								CTLTYPE_INT,
							},
							{
								"semmns",
								CTLTYPE_INT,
							},
							{
								"semmnu",
								CTLTYPE_INT,
							},
							{
								"semmsl",
								CTLTYPE_INT,
							},
							{
								"semopm",
								CTLTYPE_INT,
							},
							{
								"semume",
								CTLTYPE_INT,
							},
							{
								"semusz",
								CTLTYPE_INT,
							},
							{
								"semvmx",
								CTLTYPE_INT,
							},
							{
								"semaem",
								CTLTYPE_INT
							}
						}
			def_constant_end
						/* system semaphore information control names */
preprocessor_conditional_end

			/* never use "#define" constant definition */
			/* never use "SEM_UNDO" name and "010000" value */
			def_constant_name:	SYSSEM_UNDO		def_constant_value:	0x2000	def_constant_end
						/* system semaphore undo */

			/* never use "#define" constant definition */
			/* never use "GETNCNT" name and "3" value */
			def_constant_name:	SYSSEM_CTRL_COUNT_GET	def_constant_value:	0x0003	def_constant_end
						/* system semaphore control count get */
			/* never use "#define" constant definition */
			/* never use "GETPID" name and "4" value */
			def_constant_name:	SYSSEM_CTRL_PROCID_GET	def_constant_value:	0x0004	def_constant_end
						/* system semaphore control process id (identifier?) get */
			/* never use "#define" constant definition */
			/* never use "GETVAL" name and "5" value */
			def_constant_name:	SYSSEM_CTRL_VALUE_GET	def_constant_value:	0x0005	def_constant_end
						/* system semaphore control value get */
			/* never use "#define" constant definition */
			/* never use "GETALL" name and "6" value */
			def_constant_name:	SYSSEM_CTRL_ALL_GET	def_constant_value:	0x0006	def_constant_end
						/* system semaphore control all get */
			/* never use "#define" constant definition */
			/* never use "GETZCNT" name and "7" value */
			def_constant_name:	SYSSEM_CTRL_ZCOUNT_GET	def_constant_value:	0x0007	def_constant_end
						/* system semaphore control z count get */
			/* never use "#define" constant definition */
			/* never use "SETVAL" name and "8" value */
			def_constant_name:	SYSSEM_CTRL_VALUE_SET	def_constant_value:	0x0008	def_constant_end
						/* system semaphore control value set */
			/* never use "#define" constant definition */
			/* never use "SETALL" name and "9" value */
			def_constant_name:	SYSSEM_CTRL_ALL_SET	def_constant_value:	0x0009	def_constant_end
						/* system semaphore control all set */

			/* never use "#define" constant definition */
			/* never use "SEM_A" name and "0200" value */
			def_constant_name:	SYSSEM_PERMS_ALTER	def_constant_value:	0x0080	def_constant_end
						/* system semaphore permissions alter */
			/* never use "#define" constant definition */
			/* never use "SEM_R" name and "0400" value */
			def_constant_name:	SYSSEM_PERMS_READ	def_constant_value:	0x0100	def_constant_end
						/* system semaphore permissions read */

preprocessor_conditional:	{	_KERNEL	}
preprocessor_conditional_start:
			/* never use "#define" constant definition */
			/* never use "SEMVMX" name and "32767" value */
			def_constant_name:	SYSSEM_VALUE_MAXIMUM	def_constant_value:	0x7fff	def_constant_end
						/* system semaphore value maximum */
			/* never use "#define" constant definition */
			/* never use "SEMAEM" name and "16384" value */
			def_constant_name:	SYSSEM_ADJOX_MAXIMUM	def_constant_value:	0x4000	def_constant_end
						/* system semaphore adjust on exit maximum */
			/* never use "#define" constant definition */
			/* never use "SEMMNI" name and "10" value */
			def_constant_name:	SYSSEM_IDS_MAXIMUM	def_constant_value:	0x000a	def_constant_end
						/* system semaphore identifiers maximum */
			/* never use "#define" constant definition */
			/* never use "SEMMNS" name and "60" value */
			def_constant_name:	SYSSEM_SYSTEM_MAXIMUM	def_constant_value:	0x003c	def_constant_end
						/* system semaphore system maximum */
			/* never use "#define" constant definition */
			/* never use "SEMUME" name and "10" value */
			def_constant_name:	SYSSEM_UEPPROC_MAXIMUM	def_constant_value:	0x000a	def_constant_end
						/* system semaphore undo entries per process maximum */
			/* never use "#define" constant definition */
			/* never use "SEMMNU" name and "30" value */
			def_constant_name:	SYSSEM_UNDO_MAXIMUM	def_constant_value:	0x001e	def_constant_end
						/* system semaphore undo maximum */
			/* never use "#define" constant definition */
			/* never use "SEMMSL" name and "60" value */
			def_constant_name:	SYSSEM_SEMPID_MAXIMUM	def_constant_value:	0x003c	def_constant_end
						/* system semaphore semaphores per id (identifier?) maximum */
			/* never use "#define" constant definition */
			/* never use "SEMOPM" name and "100" value */
			def_constant_name:	SYSSEM_OPPCALL_MAXIMUM	def_constant_value:	0x0064	def_constant_end
						/* system semaphore operations per call maximum */
			/* never use "#define" constant definition */
			/* never use "SEMUSZ" name and "sizeof (struct sem_undo) + sizeof (struct undo) * SEMUME" value */
			def_constant_name:	SYSSEM_UNDO_SIZE
			def_constant_value:	SYSSEM_UEPPROC_MAXIMUM * (
							sizeof (short) * 03 + sizeof (int) * 01
						) + (
							sizeof (struct process *) * 02 + sizeof (short *) * 03 + sizeof (short) * 03 +
								sizeof (int *) * 01 + sizeof (int) * 01
						)
						/* system semaphore undo size */
			def_constant_end
preprocessor_conditional_end

			/* never use "semun" tag */
			union_tag:	syssem_union_t
			union_name:
			union_start:
				/* never use "array" name */
				struct_tag:
				struct_name:	array_s
				struct_start:
					/* never use "unsigned short" data type */
					/* it is a pointer */
					data_declaration_type: u_int16_t
					data_declaration_name: pointer *
					data_declaration_end
				struct_end

				struct_tag:
				struct_name:	data_s
				struct_start:
					/* never use "val" name */
					/* never use "int" data type */
					data_declaration_type: int32_t
					data_declaration_name: value
					data_declaration_end
				struct_end

				/* never use "buf" name */
				/* never use "semid_ds" tag */
				/* it is a pointer */
				struct_tag:	syssem_id_datastruct_t
				struct_name:	buffer_s *
				struct_start:
					struct_tag:
					struct_name:	ipc_s
					struct_start:
						/* never use "sem_perm" name */
						/* it is a redeclaration of "ipc_perm" structure */
						struct_tag:	<redeclaration of: ipc_perm>
						struct_name:	permission_s
						struct_start_and_end
					struct_end

					/* never use "sem_base" name */
					struct_tag:
					struct_name:	base_s
					struct_start:
						/* never use "sem" tag */
						/* it is a pointer */
						struct_tag:	symsem_t
						struct_name:	pointer_s *
						struct_start:
							struct_tag:
							struct_name:	data_s
							struct_start:
								/* never use "semval" name */
								/* never use "unsigned short" data type */
								data_declaration_type: u_int16_t
								data_declaration_name: value
								data_declaration_end
							struct_end

							struct_tag:
							struct_name:	process_s
							struct_start:
								/* never use "sempid" name */
								/* never use "pid_t" data type */
								data_declaration_type: int32_t
								data_declaration_name: id
								data_declaration_end
							struct_end

							struct_tag:
							struct_name:	awaiting_count_s
							struct_start:
								/* never use "semncnt" name */
								/* never use "unsigned short" data type */
								data_declaration_type: u_int16_t
								data_declaration_name: number
								data_declaration_end

								/* never use "semzcnt" name */
								/* never use "unsigned short" data type */
								data_declaration_type: u_int16_t
								data_declaration_name: zero
								data_declaration_end
							struct_end
						struct_end
					struct_end

					struct_tag:
					struct_name:	syssems_s
					struct_start:
						/* never use "sem_nsems" name */
						/* never use "unsigned short" data type */
						data_declaration_type: u_int16_t
						data_declaration_name: number
						data_declaration_end
					struct_end

					struct_tag:
					struct_name:	last_operation_s
					struct_start:
						/* never use "sem_otime" name */
						/* never use "time_t" data type */
						data_declaration_type: int64_t
						data_declaration_name: time
						data_declaration_end
					struct_end

					struct_tag:
					struct_name:	pad_first_s
					struct_start:
						/* never use "sem_pad1" name */
						data_declaration_type: long
						data_declaration_name: svabi
						data_declaration_end
					struct_end

					struct_tag:
					struct_name:	last_change_s
					struct_start:
						/* never use "sem_ctime" name */
						/* never use "time_t" data type */
						data_declaration_type: int64_t
						data_declaration_name: time
						data_declaration_end
					struct_end

					struct_tag:
					struct_name:	pad_s
					struct_start:
						/* never use "sem_pad2" name */
						data_declaration_type: long
						data_declaration_name: svabi
						data_declaration_end
					struct_end

					/* it is an array */
					struct_tag:
					struct_name:	pad_last_s[4]
					struct_start:
						/* never use "sem_pad3" name */
						data_declaration_type: long
						data_declaration_name: svabi
						data_declaration_end
					struct_end
				struct_end
			union_end

			/* never use "sembuf" tag */
			struct_tag:	syssem_buffer_t
			struct_name:
			struct_start:
				struct_tag:
				struct_name:	data_s
				struct_start:
					/* never use "sem_num" name */
					/* never use "unsigned short" data type */
					data_declaration_type: u_int16_t
					data_declaration_name: number
					data_declaration_end
				struct_end

				struct_tag:
				struct_name:	operation_s
				struct_start:
					/* never use "sem_op" name */
					/* never use "short" data type */
					data_declaration_type: int16_t
					data_declaration_name: value
					data_declaration_end

					/* never use "sem_flg" name */
					/* never use "short" data type */
					data_declaration_type: int16_t
					data_declaration_name: flags
					data_declaration_end
				struct_end
			struct_end

preprocessor_conditional:	{	_KERNEL	}
preprocessor_conditional_start:
			/* never use "sembuf" tag */
			struct_tag:	syssem_undo_t
			struct_name:
			struct_start:
				/* never use "un_next" name */
				struct_tag:
				struct_name:	next_s
				struct_start:
					struct_tag:
					struct_name:	singly_lnlste_s	/* "singly_linked_list_entry_s" */
					struct_start:
						/* it is a pointer */
						/* it is a redeclaration of "syssem_undo_t" structure */
						struct_tag:	<redeclaration of: syssem_undo_t>
						struct_name:	pointer_s *
						struct_start:
						struct_end
					struct_end
				struct_end

				/* never use "un_proc" name */
				struct_tag:
				struct_name:	process_s
				struct_start:
					/* it is a pointer */
					/* it is a redeclaration of "process" structure */
					struct_tag:	<redeclaration of: process>
					struct_name:	pointer_s *
					struct_start_and_end
				struct_end

				struct_tag:
				struct_name:	data_s
				struct_start:
					/* never use "un_cnt" name */
					/* never use "short" data type */
					data_declaration_type: int16_t
					data_declaration_name: count
					data_declaration_end
				struct_end

				/* never use "un_ent" name */
				/* never use "undo" tag */
				struct_tag:	syssem_undoe_t	/* "syssem_undo_entries_t" */
				struct_name:	entries_s
				struct_start:
					/* it is an array */
					struct_tag:
					struct_name:	data_s[1]
					struct_start:
						/* never use "un_adjval" name */
						/* never use "short" data type */
						data_declaration_type: int16_t
						data_declaration_name: adjust_on_exit
						data_declaration_end

						/* never use "un_num" name */
						/* never use "short" data type */
						data_declaration_type: int16_t
						data_declaration_name: number
						data_declaration_end

						/* never use "un_id" name */
						/* never use "int" data type */
						data_declaration_type: int32_t
						data_declaration_name: id
						data_declaration_end
					struct_end
				struct_end
			struct_end

			/* never use "sem_sysctl_info" tag */
			struct_tag:	syssem_sysctrl_t	/* "syssem_systemcontrol_t" */
			struct_name:
			struct_start:
				struct_tag:
				struct_name:	data_s
				struct_start:
					/* never use "seminfo" name */
					/* never use "seminfo" tag */
					struct_tag:	syssem_info_t	/* "syssem_information_t" */
					struct_name:	information_s
					struct_start:
						struct_tag:
						struct_name:	maximum_n_s	/* "maximum_number_s" */
						struct_start:
							/* never use "semmni" data type */
							/* never use "int" data type */
							data_declaration_type: int32_t
							data_declaration_name:	identifiers
							data_declaration_end

							/* never use "semmns" name */
							/* never use "imt" data type */
							data_declaration_type: int32_t
							data_declaration_name: system
							data_declaration_end

							/* never use "semmnu" data type */
							/* never use "int" data type */
							data_declaration_type: int32_t
							data_declaration_name: undo
							data_declaration_end

							/* never use "semmsl" name */
							/* never use "int" data type */
							data_declaration_type: int32_t
							data_declaration_name: sems_per_id	/* "semaphores_per_id" */
							data_declaration_end

							/* never use "semopm" name */
							/* never use "int" data type */
							data_declaration_type: int32_t
							data_declaration_name: ops_per_call	/* "operations_per_call" */
							data_declaration_end

							/* never use "semume" name */
							/* never use "int" data type */
							data_declaration_type: int32_t
							data_declaration_name: ues_per_proc	/* "undo_entries_per_process" */
							data_declaration_end
						struct_end

						struct_tag:
						struct_name:	size_s
						struct_start:
							/* never use "semusz" name */
							/* never use "int" data type */
							data_declaration_type: int32_t
							data_declaration_name: undo
							data_declaration_end
						struct_end

						struct_tag:
						struct_name:	maximum_s
						struct_start:
							/* never use "semvmx" name */
							/* never use "int" data type */
							data_declaration_type: int32_t
							data_declaration_name: value
							data_declaration_end

							/* never use "semaem" name */
							/* never use "int" data type */
							data_declaration_type: int32_t
							data_declaration_name: adjust_on_exit
							data_declaration_end
						struct_end
					struct_end
				struct_end

				struct_tag:
				struct_name:	structure_s
				struct_start:
					/* never use "semids" name */
					/* it is an array */
					/* it is a redeclaration of "syssem_id_datastruct_t" structure */
					struct_tag:	<redeclaration of: syssem_id_datastruct_t>
					struct_name:	id_data_s[1]
					struct_start_and_end
				struct_end
			struct_end

			/* require "extern" data type */
			/* never use "seminfo" name */
			// extra_data_type: extern
			struct_tag:
			struct_name:	syssem_info_s	/* "syssem_information_s" */
			struct_start:
				/* it is a redeclaration of "syssem_info_t" structure */
				struct_tag:	<redeclaration of: syssem_info_t>
				struct_name:	data_s
				struct_start_and_end
			struct_end

			/* require "extern" data type */
			/* never use "sema" name */
			// extra_data_type: extern
			struct_tag:
			struct_name:	syssem_idlist_s
			struct_start:
				/* it is a double pointer */
				/* it is a redeclaration of "syssem_id_datastruct_t" structure */
				struct_tag:	<redeclaration of: syssem_id_datastruct_t>
				struct_name:	dpointer_s **
				struct_start_and_end
			struct_end

			/* functions declarations */
preprocessor_conditional_else:

/* "__BEGIN_DECLS" and "__END_DECLS" are used in C++ as C extern */
preprocessor_set:	__BEGIN_DECLS
preprocessor_set_end
			/* functions declarations */
preprocessor_set:	__END_DECLS
preprocessor_set_end

preprocessor_conditional_end

	algorithm_end

// code_end
