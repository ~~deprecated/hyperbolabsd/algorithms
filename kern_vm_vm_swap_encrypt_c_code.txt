// file_name:		uvm/uvm_swe.c	/* never use "uvm/uvm_swap_encrypt.c" */
// file_name_end

// code_name:		Virtual memory, swap encrypt
// code_name_end

// required_headers:	#include <sys/errno.h>		/* required by: ENOTDIR and EOPNOTSUPP */
//			#include <sys/sysctl.h>		/* required by: u_char, sysctl_int() and sysctl_rdint() */
//			#include <crypto/rijndael.h>	/* required by: rijndael_ctx and rijndael_encrypt() */
//			#include <machine/mainproc.h>	/* required by: (NOTE: It is under development) */
//			#include <uvm/uvm_extern.h>	/* required by: struct vm_page and boolean_t */
//			#include <uvm/uvm_swap.h>	/* required by: uvm_swap_initcrypt_all() */
//			#include <uvm/uvm_swe.h>	/* required by: struct {} uvm_swap_encrypt_s, struct uvm_swap_encrypt_key_t, UVM_SWAPENC_ENABLE, UVM_SWAPENC_CREATED and UVM_SWAPENC_DELETED */
// required_headers_end

// code:

	algorithm:	struct_tag:
			struct_name:	uvm_swap_encrypt_keys_s
			struct_start:
				struct_tag:
				struct_name:	data_s
				struct_start:
					/* never use "uvm_swpkeyscreated" name */
					/* never use "u_int" data type */
					/* never use "0" value */
					data_dec_and_def_type:	u_int32_t
					data_dec_and_def_name:	created
					data_dec_and_def_value:	00
					data_dec_and_def_end

					/* never use "uvm_swpkeysdeleted" name */
					/* never use "u_int" data type */
					/* never use "0" value */
					data_dec_and_def_type:	u_int32_t
					data_dec_and_def_name:	deleted
					data_dec_and_def_value:	00
					data_dec_and_def_end
				struct_end
			struct_end

			/* never use "swap_ctxt" name */
			/* it is a structure */
			data_declaration_type:	rijndael_ctx
			data_declaration_name:	uvm_swap_encrypt_ctxt_s
			data_declaration_end

			/* never use "kcur" name */
			/* it is a pointer */
			/* it is a redeclaration of "uvm_swap_encrypt_key_t" structure */
			struct_tag:	<redeclaration of: uvm_swap_encrypt_key_t>
			struct_name:	uvm_swap_encrypt_kcur_ptr_s *
			struct_start_and_end

			/* never use "oldp" VOID pointer parameter; use "old_ptr" VOID pointer parameter instead */
			/* never use "oldlenp" pointer parameter and "size_t *" data type;
			   use "old_lenght_ptr" pointer parameter and "unsigned long *" data type instead */
			/* never use "newp" VOID pointer parameter; use "new_ptr" VOID pointer parameter instead */
			/* never use "newlen" variable parameter and "size_t" data type;
			   use "new_lenght" variable parameter and "unsigned long" data type instead */
			/* never use "p" structure pointer parameter; it is an unused "proc *" structure pointer parameter */
			/* never use "name" pointer parameter and "int *" data type;
			   use "name_a" pointer parameter and "int32_t *" data type instead */
			/* never use "namelen" variable parameter and "u_int" data type;
			   use "name_lenght" variable parameter and "u_int32_t" data type instead */
			/* use "ectr_ptr" pointer parameter and "int32_t *" data type */
			/* use "void" data type in function return */
			LOCAL 1) uvm_swe_control	/* never use "swap_encrypt_ctl" name */

			algorithm:	/* never use "if" conditional statement */
					/* never use "namelen != 1" conditional */
					conditional:		{	name_lenght == 01	}
					conditional_start:
						/* nothing, continue */
					conditional_else:
						*ectr_ptr = ENOTDIR
						return
					conditional_end

					/* never use "switch" conditional statement */
					/* never use "name[0] == SWPENC_ENABLE" conditional */
					conditional:		{	n[0] == UVM_SWAPENC_ENABLE	}
					conditional_start:
						struct_tag:
						struct_name:	swapenc_s
						struct_start:
							/* never use "doencrypt" name */
							/* never use "int" data type */
							data_declaration_type:	int32_t
							data_declaration_name:	do_enc
							data_declaration_end

							/* never use "result" name */
							/* never use "int" data type */
							data_declaration_type:	int32_t
							data_declaration_name:	rslt
							data_declaration_end
						struct_end

						/* never use "int doencrypt = uvm_doswapencrypt" */
						data_definition_name:	swapenc_s.do_enc
						data_definition_value:	uvm_swap_encrypt_s.data_s.do_swap_encrypt
						data_definition_end

						/* never use "result = sysctl_int(oldp, oldlenp, newp, newlen, &doencrypt)" */
						data_definition_name:	swapenc_s.rslt
						data_definition_value:	sysctl_int(old_ptr, old_lenght_ptr, new_ptr, new_lenght, &swapenc_s.do_enc)
						data_definition_end

						/* never use "if" conditional statement */
						/* never use "doencrypt" conditional */
						conditional:		{	swapenc_s.do_enc	}
						conditional_start:
							uvm_swap_initcrypt_all()
						conditional_end

						/* never use "if" conditional statement */
						/* never use "result" conditional */
						conditional:		{	swapenc_s.rslt	}
						conditional_start:
							*ectr_ptr = swapenc_s.rslt
							return
						conditional_end

						/* never use "uvm_doswapencrypt = doencrypt" */
						data_definition_name:	uvm_swap_encrypt_s.data_s.do_swap_encrypt
						data_definition_value:	swapenc_s.do_enc
						data_definition_end

						*ectr_ptr = 0;
						return;
					/* never use "name[0] == SWPENC_CREATED" conditional */
					conditional_elif:	{	name_a[0] == UVM_SWAPENC_CREATED	}
					conditional_start:
						/* never use "return (sysctl_rdint(oldp, oldlenp, newp, uvm_swpkeyscreated))" */
						*ectr_ptr = sysctl_rdint(old_ptr, old_lenght_ptr, new_ptr, uvm_swap_encrypt_keys_s.data_s.created)
						return
					/* never use "name[0] == SWPENC_DELETED" conditional */
					conditional_elif:	{	name_a[0] == UVM_SWAPENC_DELETED	}
					conditional_start:
						/* never use "return (sysctl_rdint(oldp, oldlenp, newp, uvm_swpkeysdeleted))" */
						*ectr_ptr = sysctl_rdint(old_ptr, old_lenght_ptr, new_ptr, uvm_swap_encrypt_keys_s.data_s.deleted)
						return
					conditional_else:
						/* never use "return (EOPNOTSUPP)" */
						*ectr_ptr = EOPNOTSUPP
						return
					conditional_end
			algorithm_end

			/* never use "encrypt" variable parameter and "int" data type;
			   use "enc" variable parameter and "int32_t" data type instead */
			/* never use "key" variable parameter and "struct swap_key *" data type;
			   use "ekey" VOID pointer parameter instead */
			/* use "void" data type in function return */
			LOCAL 2) uvm_swe_swap_key_prepare	/* never use "swap_key_prepare" name */

			algorithm:	struct_dec_and_def_tag:		uvm_swap_encrypt_key_t *
					struct_dec_and_def_name:	ekey_ptr
					struct_dec_and_def_start:	ekey
					struct_dec_and_def_end

					struct_dec_and_def_tag:		rijndael_ctx *
					struct_dec_and_def_name:	sectx
					struct_dec_and_def_start:	&uvm_swap_encrypt_ctxt_s
					struct_dec_and_def_end

					/* never use "if" conditional statement */
					/* never use "kcur == key && (encrypt || !swap_ctxt.enc_only)" conditional */
					conditional:		{	(!sectx->enc_only || enc) == 00	}
					conditional_start:
						/* nothing, continue */
					conditional_else:
						conditional:		{	(uvm_swap_encrypt_key_current_s == ekey_ptr)	}
						conditional_start:
									return
						conditional_end
					conditional_end

					/* never use "if" conditional statement */
					/* never use "encrypt" conditional */
					conditional:		{	enc == 0	}
					conditional_start:
						rijndael_set_key(sectx, (const u_int8_t *)ekey_ptr->data_s.key_a, sizeof(ekey_ptr->data_s.key_a) * 010)
					conditional_else:
						rijndael_set_key_enc_only(sectx, (const u_int8_t *)ekey_ptr->data_s.key_a, sizeof(ekey_ptr->data_s.key_a) * 010)
					conditional_end

					/* never use "kcur = key" */
					data_definition_name:	uvm_swap_encrypt_key_current_s
					data_definition_value:	ekey_ptr
					data_definition_end
			algorithm_end

			/* never use "key" structure pointer parameter and "struct swap_key" data type;
			   use "keys" array pointer parameter and "unsigned int *" data type instead */
			/* use "void" data type in function return */
			LOCAL 3) uvm_swe_swap_key_create	/* never use "swap_key_create" name */

			algorithm:	/* never use "arc4random_buf(key->key, sizeof(key->key))" */
					arc4random_buf(keys, sizeof(keys))

					/* never use "uvm_swpkeyscreated++" */
					data_definition_name:	uvm_swap_encrypt_keys_s.data_s.created
					data_definition_value:	+ 01
					data_definition_end
			algorithm_end

			/* never use "key" variable parameter and "struct swap_key *" data type;
			   use "ekey" VOID pointer parameter instead */
			/* use "void" data type in function return */
			LOCAL 4) uvm_swe_swap_key_cleanup	/* never use "swap_key_cleanup" name */

			algorithm:	struct_dec_and_def_tag:		uvm_swap_encrypt_key_t *
					struct_dec_and_def_name:	ekey_ptr
					struct_dec_and_def_start:	ekey
					struct_dec_and_def_end

					struct_dec_and_def_tag:		rijndael_ctx *
					struct_dec_and_def_name:	sectx
					struct_dec_and_def_start:	&uvm_swap_encrypt_ctxt_s
					struct_dec_and_def_end

					/* never use "kcur" name */
					/* it is a pointer */
					/* it is a redeclaration of "uvm_swap_encrypt_key_t" structure */
					struct_tag:	<redeclaration of: uvm_swap_encrypt_key_t>
					struct_name:	uvm_swap_encrypt_key_null_s *
					struct_start_and_end

					/* never use "if" conditional statement */
					/* never use "kcur == NULL || kcur != key" conditional */
					conditional:		{	uvm_swap_encrypt_key_current_s != ekey_ptr	}
					conditional_start:
								return
					conditional_end

					/* never use "explicit_bzero(&swap_ctxt, sizeof(swap_ctxt))" */
					explicit_bzero(sectx, sizeof(*sectx))

					/* never use "kcur = NULL" */
					data_definition_name =	uvm_swap_encrypt_key_current_s
					data_definition_value =	uvm_swap_encrypt_key_null_s
					data_definition_end
			algorithm_end

			/* never use "key" structure pointer parameter and "struct swap_key *" data type instead ;
			   use "keys" structure pointer parameter and "unsigned int *" data type instead */
			/* use "void" data type in function return */
			LOCAL 5) uvm_swe_swap_key_delete	/* never use "swap_key_delete" name */

			algorithm:	/* never use "swap_key_cleanup(key)" */
					uvm_swe_swap_key_cleanup((void *)keys)

					/* never use "explicit_bzero(key, sizeof(*key))" */
					explicit_bzero(keys, sizeof(*keys))

					/* never use "uvm_swpkeysdeleted++" */
					data_definition_name:	uvm_swap_encrypt_keys_s.data_s.deleted
					data_definition_value:	+ 01
					data_definition_end
			algorithm_end

			/* note: this algorithm comes from uvm/uvm_swap_encrypt.h */
			/* never use "#define" constant definition */
			/* never use "x" structure pointer parameter; use "ekey" structure pointer parameter and "struct uvm_swap_encrypt_key_t *" data type instead */
			/* never use "s" structure pointer parameter; it is an unused "uvm_swap_encrypt_key_t *" structure pointer parameter */
			/* use "unsigned short" data type in function return */
			LOCAL 6) uvm_swe_swap_key_get	/* never use "SWAP_KEY_GET" name */

			algorithm:	/* never use "if" conditional statement */
					/* never use "x->refcount == 0" conditional */
					conditional:		{	ekey->data_s.reference_count == 00	}
					conditional_start:
						uvm_swe_swap_key_create(ekey->data_s.key_a)
					conditional_end

					struct_dec_and_def_tag:		unsigned short
					struct_dec_and_def_name:	rvar
					struct_dec_and_def_start:	ekey->data_s.reference_count
					struct_dec_and_def_end

					/* never use "x->refcount++" */
					data_definition_name:	ekey->data_s.reference_count
					data_definition_value:	+ 01
					data_definition_end

					return rvar
			algorithm_end

			/* note: this algorithm comes from uvm/uvm_swap_encrypt.h */
			/* never use "#define" constant definition */
			/* never use "x" structure pointer parameter; use "ekey" structure pointer parameter and "struct uvm_swap_encrypt_key_t *" data type instead */
			/* never use "s" structure pointer parameter; it is an unused "struct uvm_swap_encrypt_key_t *" structure pointer parameter */
			/* use "unsigned short" data type in function return */
			LOCAL 7) uvm_swe_swap_key_put	/* never use "SWAP_KEY_PUT" name */

			algorithm:	struct_dec_and_def_tag:		unsigned short
					struct_dec_and_def_name:	rvar
					struct_dec_and_def_start:	ekey->data_s.reference_count
					struct_dec_and_def_end

					/* never use "x->refcount--" */
					data_definition_name:	ekey->data_s.reference_count
					data_definition_value:	- 01
					data_definition_end

					/* never use "if" conditional statement */
					/* never use "x->refcount == 0" conditional */
					conditional:		{	ekey->data_s.reference_count == 00	}
					conditional_start:
						uvm_swe_swap_key_delete(ekey->data_s.key_a)
					conditional_end
			algorithm_end

			/* never use "key" structure pointer parameter and "struct swap_key *" data type; 
			   use "ekey" structure pointer parameter and "struct uvm_swap_encrypt_key_t *" data type instead */
			/* never use "src" variable parameter and "caddr_t" data type;
			   use "source" variable parameter and "char *" data type instead */
			/* never use "dst" variable parameter and "caddr_t" data type;
			   use "destination" variable parameter and "char *" data type instead */
			/* never use "block" variable parameter and "u_int64_t" data type;
			   use "blk" variable parameter and "unsigned long long" data type;
			/* never use "count" variable parameter and "size_t" data type;
			   use "cnt" variable parameter and "unsigned long" data type instead */
			/* use "void" data type in function return */
			LOCAL 8) uvm_swe_encrypt		/* never use "swap_encrypt" name */

			algorithm:	struct_dec_and_def_tag:		rijndael_ctx *
					struct_dec_and_def_name:	sectx
					struct_dec_and_def_start:	&uvm_swap_encrypt_ctxt_s
					struct_dec_and_def_end

					/* never use "if" conditional statement */
					/* never use "!swap_encrypt_initialized" conditional statement */
					conditional:		{	uvm_swap_encrypt_s.data_s.swap_encrypt_initd == 00	}
					conditional_start:
						/* never use "swap_encrypt_initialized = 1" */
						uvm_swap_encrypt_s.data_s.swap_encrypt_initd = 01
					conditional_end

					struct_tag:
					struct_name:	swapenc_s
					struct_start:
						/* never use "dsrc" and "ddst" names */
						/* never use "u_int32_t" data type */
						/* it is a pointer */
						data_declaration_type:	u_int *
						data_declaration_name:	d_a[2]
						data_declaration_end:

						/* never use "iv" name */
						/* never use "u_int32_t" data type */
						data_declaration_type:	u_int
						data_declaration_name:	iv_a[4]
						data_declaration_end
					struct_end

					/* never use "count /= sizeof(u_int32_t)" */
					data_definition_name:	swapenc_s.count
					data_definition_value:	cnt / sizeof(u_int)
					data_definition_end

					/* never use "u_int32_t *dsrc = (u_int32_t *)src" */
					data_definition_name:	swapenc_s.d_a[0]
					data_definition_value:	(u_int *)source
					data_definition_end

					/* never use "u_int32_t *ddst = (u_int32_t *)dst" */
					data_definition_name:	swapenc_s.d_a[1]
					data_definition_value:	(u_int *)destination;
					data_definition_end

					/* never use "iv[0] = block >> 32" */
					data_definition_name:	swapenc_s.iv_a[0]
					data_definition_value:	(blk >> 040)
					data_definition_end

					/* never use "iv[1] = block" */
					data_definition_name:	swapenc_s.iv_a[1]
					data_definition_value:	(blk >> 000)
					data_definition_end

					/* never use "iv[2] = ~iv[0]" */
					data_definition_name:	swapenc_s.iv_a[2]
					data_definition_value:	~ (blk >> 040)
					data_definition_end

					/* never use "iv[3] = ~iv[1]" */
					data_definition_name:	swapenc_s.iv_a[3]
					data_definition_value:	~ (blk >> 000)
					data_definition_end

					/* never use "swap_key_prepare(key, 1)" */
					uvm_swe_swap_key_prepare(01, (void *)ekey)

					/* never use "rijndael_encrypt(&swap_ctxt, (u_char *)iv, (u_char *)iv)" */
					rijndael_encrypt(sectx, (u_int8_t *)swapenc_s.iv_a, (u_int8_t *)swapenc_s.iv_a)

					/* never use "for" conditional loop */
					loop_conditional:	row0_start:		{	/* nothing, "swapenc_s.count" is variable parameter defined */	}
								row0_conditional:	/* never use "count > 0" */
											{	00 < swapenc_s.count	}
								row0_changing:		/* never use "count -= 4" */
										{	swapenc_s.count = swapenc_s.count - 04	}
					loop_conditional_start:

						/* never use "ddst[0] = dsrc[0] ^ iv1" */
						data_definition_name:	swapenc_s.d_a[0][0]
						data_definition_value:	swapenc_s.iv_a[0] ^ swapenc_s.d_a[1][0]
						data_definition_end

						/* never use "ddst[1] = dsrc[1] ^ iv2" */
						data_definition_name:	swapenc_s.d_a[0][1]
						data_definition_value:	swapenc_s.iv_a[1] ^ swapenc_s.d_a[1][1]
						data_definition_end

						/* never use "ddst[2] = dsrc[2] ^ iv3" */
						data_definition_name:	swapenc_s.d_a[0][2]
						data_definition_value:	swapenc_s.iv_a[2] ^ swapenc_s.d_a[1][2]
						data_definition_end

						/* never use "ddst[3] = dsrc[3] ^ iv4" */
						data_definition_name:	swapenc_s.d_a[0][3]
						data_definition_value:	swapenc_s.iv_a[3] ^ swapenc_s.d_a[1][3]
						data_definition_end

						/* never use "rijndael_encrypt(&swap_ctxt, (u_char *)ddst, (u_char *)ddst)" */
						rijndael_encrypt(sectx, (u_int8_t *)swapenc_s.d_a[1], (u_int8_t *)swapenc_s.d_a[1])

						/* never use "iv1 = ddst[0]" */
						data_definition_name:	swapenc_s.iv_a[0]
						data_definition_value:	swapenc_s.d_a[0][0]
						data_definition_end

						/* never use "iv2 = ddst[1]" */
						data_definition_name:	swapenc_s.iv_a[1]
						data_definition_value:	swapenc_s.d_a[0][1]
						data_definition_end

						/* never use "iv3 = ddst[2]" */
						data_definition_name:	swapenc_s.iv_a[2]
						data_definition_value:	swapenc_s.d_a[0][2]
						data_definition_end

						/* never use "iv4 = ddst[3]" */
						data_definition_name:	swapenc_s.iv_a[3]
						data_definition_value:	swapenc_s.d_a[0][3]
						data_definition_end

						/* never use "dsrc += 4" */
						data_definition_name:	swapenc_s.d_a[0]
						data_definition_value:	04 + swapenc_s.d_a[0]
						data_definition_end

						/* never use "ddst += 4" */
						data_definition_name:	swapenc_s.d_a[1]
						data_definition_value:	04 + swapenc_s.d_a[1]
						data_definition_end
					loop_conditional_end
			algorithm_end

			/* never use "key" structure pointer parameter and "struct swap_key *" data type;
			   use "ekey" structure pointer parameter and "struct uvm_swap_encrypt_key_t *" data type instead */
			/* never use "src" variable parameter and "caddr_t" data type;
			   use "source" variable pointer parameter and "char" data type */
			/* never use "dst" variable parameter and "caddr_t" data type;
			   use "destination" variable pointer parameter and "char" data type */
			/* never use "block" variable parameter and "u_int64_t" data type;
			   use "blk" variable parameter and "unsigned long long" data type */
			/* never use "count" variable parameter and "size_t" data type;
			   use "cnt" variable parameter and "unsigned long" data type */
			/* use "void" data type in function return */
			LOCAL 9) uvm_swe_decrypt		/* never use "swap_decrypt" name */

			algorithm:	struct_dec_and_def_tag:		rijndael_ctx *
					struct_dec_and_def_name:	sectx
					struct_dec_and_def_start:	&uvm_swap_encrypt_ctxt_s
					struct_dec_and_def_end

					/* never use "if" conditional statement */
					/* never use "!swap_encrypt_initialized" conditional statement */
					conditional:		{	uvm_swap_encrypt_s.data_s.swap_encrypt_initd == 00	}
					conditional_start:
						/* never use "panic("swap_decrypt: key not initialized")" */
						panic("uvm_swe_decrypt: isn't started the key")
					conditional_end

					
					struct_tag:
					struct_name: swapenc_s
					struct_start:
						/* never use "count /= sizeof(u_int32_t)" */
						data_declaration_type:	unsigned long
						data_declaration_name:	count
						data_declaration_end

						/* never use "dsrc" and "ddst" names */
						/* never use "u_int32_t" data type */
						/* it is a pointer */
						data_declaration_type:	u_int *
						data_declaration_name:	d_a[2]
						data_declaration_end

						/* never use "iv" name */
						/* never use "u_int32_t" data type */
						data_declaration_type:	u_int
						data_declaration_name:	iv_a[4]
						data_declaration_end

						/* never use "niv" name */
						/* never use "u_int32_t" data type */
						data_declaration_type:	u_int
						data_declaration_name:	niv_a[4]
						data_declaration_end
					struct_end

					/* never use "count /= sizeof(u_int32_t)" */
					data_definition_type:	swapenc_s.count
					data_definition_name:	cnt / sizeof(u_int)
					data_definition_end

					/* never use "u_int32_t *dsrc = (u_int32_t *)src" */
					data_definition_type:	swapenc_s.d_a[0]
					data_definition_name:	(u_int *)source
					data_definition_end

					/* never use "u_int32_t *ddst = (u_int32_t *)dst" */
					data_definition_type:	swapenc_s.d_a[1]
					data_definition_name:	(u_int *)destination
					data_definition_end

					/* never use "iv[0] = block >> 32" */
					data_definition_type:	swapenc_s.iv_a[0]
					data_definition_name:	(blk >> 040)
					data_definition_end

					/* never use "iv[1] = block" */
					data_definition_type:	swapenc_s.iv_a[1]
					data_definition_name:	(blk >> 000)
					data_definition_end

					/* never use "block; iv[2] = ~iv[0]" */
					data_definition_type:	swapenc_s.iv_a[2]
					data_definition_name:	~ (blk >> 040)
					data_definition_end

					/* never use "iv[3] = ~iv[1]" */
					data_definition_type:	swapenc_s.iv_a[3]
					data_definition_name:	~ (blk >> 000)
					data_definition_end

					/* never use "swap_key_prepare(key, 0)" */
					uvm_swe_swap_key_prepare(00, (void *)ekey)

					/* never use "rijndael_encrypt(&swap_ctxt, (u_char *)iv, (u_char *)iv)" */
					rijndael_encrypt(sectx, (u_int8_t *)swapenc_s.iv_a, (u_int8_t *)swapenc_s.iv_a)

					/* never use "for" conditional loop */
					loop_conditional:	row0_start:		{	/* nothing, "swapenc_s.count" is variable parameter defined */	}
								row0_conditional:	/* never use "count > 0" */
											{	00 < swapenc_s.count	}
								row0_changing:		/* never use "count -= 4" */
											{	swapenc_s.count = swapenc_s.count - 04	}
					loop_conditional_start:

						/* never use "ddst[0] = niv1 = dsrc[0]" */
						data_definition_name:	swapenc_s.d_a[0][0]
						data_definition_value:	swapenc_s.niv_a[0]
						data_definition_end

						data_definition_name:	swapenc_s.d_a[0][1]
						data_definition_value:	swapenc_s.niv_a[1]
						data_definition_end

						/* never use "ddst[1] = niv2 = dsrc[1]" */
						data_definition_name:	swapenc_s.d_a[0][2]
						data_definition_value:	swapenc_s.niv_a[2]
						data_definition_end

						data_definition_name:	swapenc_s.d_a[0][3]
						data_definition_value:	swapenc_s.niv_a[3]
						data_definition_end

						/* never use "ddst[2] = niv3 = dsrc[2]" */
						data_definition_name:	swapenc_s.niv_a[0]
						data_definition_value:	swapenc_s.d_a[1][0]
						data_definition_end

						data_definition_name:	swapenc_s.niv_a[1]
						data_definition_value:	swapenc_s.d_a[1][1]
						data_definition_end

						/* never use "ddst[3] = niv4 = dsrc[3]" */
						data_definition_name:	swapenc_s.niv_a[2]
						data_definition_value:	swapenc_s.d_a[1][2]
						data_definition_end

						data_definition_name:	swapenc_s.niv_a[3]
						data_definition_value:	swapenc_s.d_a[1][3]
						data_definition_end

						/* never use "rijndael_decrypt(&swap_ctxt, (u_char *)ddst, (u_char *)ddst)" */
						rijndael_encrypt(sectx, (u_int8_t *)swapenc_s.d_a[1], (u_int8_t *)swapenc_s.d_a[1])

						/* never use "ddst[0] ^= iv1" */
						data_definition_name:	swapenc_s.d_a[0][0]
						data_definition_value:	swapenc_s.iv_a[0] ^ swapenc_s.d_a[0][0]
						data_definition_end

						/* never use "ddst[1] ^= iv2" */
						data_definition_name:	swapenc_s.d_a[0][1]
						data_definition_value:	swapenc_s.iv_a[1] ^ swapenc_s.d_a[0][1]
						data_definition_end

						/* never use "ddst[2] ^= iv3" */
						data_definition_name:	swapenc_s.d_a[0][2]
						data_definition_value:	swapenc_s.iv_a[2] ^ swapenc_s.d_a[0][2]
						data_definition_end

						/* never use "ddst[3] ^= iv4" */
						data_definition_name:	swapenc_s.d_a[0][3]
						data_definition_value:	swapenc_s.iv_a[3] ^ swapenc_s.d_a[0][3]
						data_definition_end

						/* never use "iv1 = niv1" */
						data_definition_name:	swapenc_s.iv_a[0]
						data_definition_value:	swapenc_s.d_a[0][0]
						data_definition_end

						/* never use "iv2 = niv2" */
						data_definition_name:	swapenc_s.iv_a[1]
						data_definition_value:	swapenc_s.d_a[0][1]
						data_definition_end

						/* never use "iv3 = niv3" */
						data_definition_name:	swapenc_s.iv_a[2]
						data_definition_value:	swapenc_s.d_a[0][2]
						data_definition_end

						/* never use "iv4 = niv4" */
						data_definition_name:	swapenc_s.iv_a[3]
						data_definition_value:	swapenc_s.d_a[0][3]
						data_definition_end

						/* never use "dsrc += 4" */
						data_definition_name:	swapenc_s.d_a[0]
						data_definition_value:	04 + swapenc_s.d_a[0]
						data_definition_end

						/* never use "ddst += 4" */
						data_definition_name:	swapenc_s.d_a[1]
						data_definition_value:	04 + swapenc_s.d_a[1]
						data_definition_end
					loop_conditional_end
			algorithm_end
	algorithm_end

// code_end
