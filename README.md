<img alt="HYPERBOLABSD_LOGOTYPE_IMAGE" src="https://git.hyperbola.info:50100/culture/hyperbolacc.git/plain/src/00/logotypes/logotype_symbol_and_text-g_hyperbola-n_hyperbolabsd_symbol_and_text-i0-u_sldstrPblr_sldshpPblr_grdshdPblr_grdlgtPblr-c_hyperbola_and_dark_text-r8910x2048px-a0f1urwgothic_s0-t_svg1d1basic.svg" text="HyperbolaBSD™ logotype image" width="512">
<img alt="HYPER_BOLA_CHARACTER_IMAGE" src="https://git.hyperbola.info:50100/culture/hyperbolacc.git/plain/src/00/drawings/drawing_character-g_hyperbola-n_hyperbola-p_standing_in_bipedal_and_leaning_his_right_hand-i1-u_sldstr_sldshp_sldshdPblr_sldlgtPblr-c_hypercolour-r2048px2-a0f0s0-t_svg1d1basic.svg" text="Hyper Bola🄯 character image" height="160">

Algorithms
==========

Recipes with descriptions and sequence of steps of different algorithms for the
_[HyperbolaBSD&trade;][HYPERBOLA]_ development.

Algorithms License:
-------------------

Public Domain _2020_. All content is released under the __[CC0 license][CC0]__.

Licenses for _logotype_ and _character_:
----------------------------------------

*   [__HyperbolaBSD&trade;__ logotype image _2017/05/12_][LOGOTYPE]<br/>
    by _[Andr&eacute; Silva][EMULATORMAN]_, _[M&aacute;rcio Silva][COADDE]_ and
    _[Jos&eacute; Silva][CRAZYTOON]_ is under the terms of either:<br/>
    _[Free Art License version 1.3][FAL]_,<br/>
    _[Creative Commons&reg; Attribution-ShareAlike 4.0 International License]
[CCBYSA]_ or<br/>
    _[GNU&reg; Free Documentation License version 1.3 with special exceptions]
[FDL+SE]_.

*   [__Hyper Bola&#127279;__ character image _2017/04_, _2019/01/22_ and
    _2019/07/10_][CHARACTER]<br/>
    by _[Jos&eacute; Silva][CRAZYTOON]_, _[M&aacute;rcio Silva][COADDE]_ and
    _[Andr&eacute; Silva][EMULATORMAN]_ is under the terms of either:<br/>
    _[Free Art License version 1.3][FAL]_,<br/>
    _[Creative Commons&reg; Attribution-ShareAlike 4.0 International License]
[CCBYSA]_ or<br/>
    _[GNU&reg; Free Documentation License version 1.3 with special exceptions]
[FDL+SE]_.


[HYPERBOLA]: https://www.hyperbola.info/
    "HyperbolaBSD™"

[LOGOTYPE]: https://git.hyperbola.info:50100/culture/hyperbolacc.git/plain/src/00/documents/information_character-g_hyperbola-n_hyperbola-t_md.md
    "HyperbolaBSD™ logotype image"
[CHARACTER]: https://git.hyperbola.info:50100/culture/hyperbolacc.git/plain/src/00/drawings/drawing_character-g_hyperbola-n_hyperbola-p_standing_in_bipedal_and_leaning_his_right_hand-i1-u_sldstr_sldshp-c_hypercolour-r2048px2-a0f0s0-t_svg1d2tiny.svg
    "Hyper Bola🄯 character image"

[COADDE]: coadde@hyperbola.info
    "Márcio Silva (Co.Ad.De.) <coadde@hyperbola.info>"
[CRAZYTOON]: crazytoon@hyperbola.info
    "José Silva (Crazytoon) <crazytoon@hyperbola.info>"
[EMULATORMAN]: emulatorman@hyperbola.info
    "André Silva (Emulatorman) <emulatorman@hyperbola.info>"

[CC0]: legalcode.txt
    "Public Domain"
[FAL]: https://artlibre.org/licence/lal/en/
    "Free Art License version 1.3"
[CCBYSA]: https://creativecommons.org/licenses/by-sa/4.0/
    "Creative Commons® Attribution-ShareAlike 4.0 International License"
[FDL+SE]: https://git.hyperbola.info:50100/culture/hyperbolacc.git/plain/COPYING_FDL_P_SE_V1_3
    "GNU® Free Documentation License version 1.3 with the special exceptions"
