// file_name:		arch/i386/include/mp_var.h	/* never use "arch/i386/include/cpuvar.h" */
// file_name_end

// code_name:		Main Processor (CPU) variables
// code_name_end

// required_headers:	#include <sys/types.h>		/* required by: int8_t and u_int32_t */
// required_headers_end

// code:

	algorithm:	/* never use "#define" constant definition */
			/* never use "CPU_ROLE_SP" name and "0" value */
			def_constant_name:	MPVAR_ROLE_SP			def_constant_value:	00	def_constant_end
			/* never use "#define" constant definition */
			/* never use "CPU_ROLE_BP" name and "1" value */
			def_constant_name:	MPVAR_ROLE_BP			def_constant_value:	01	def_constant_end
			/* never use "#define" constant definition */
			/* never use "CPU_ROLE_AP" name and "2" value */
			def_constant_name:	MPVAR_ROLE_AP			def_constant_value:	02	def_constant_end
			/* never use "#define" constant definition */
			/* never use "MP_PICMODE" name and "0x00000001" value */
			def_constant_name:	MPVAR_MP_PIC_MODE		def_constant_value:	01	def_constant_end

			/* structure (with function pointers) data type definition */

			/* structure (with function pointers) declaration */

			/* never use "cpu_attach_args" tag */
			struct_tag:	mpvar_attach_args_t
			struct_name:
			struct_start:
				struct_tag:
				struct_name:	caa_name_s
				struct_start:
					/* never use "caa_name" name */
					/* never use "char *" data type */
					data_declaration_type: int8_t *
					data_declaration_name: pointer
					data_declaration_end
				struct_end

				struct_tag:
				struct_name:	data_s
				struct_start:
					/* never use "cpu_apicid" name */
					/* never use "int" data type */
					data_declaration_type: int32_t
					data_declaration_name: apic_id
					data_declaration_end

					/* never use "cpu_acpi_proc_id" name */
					/* never use "int" data type */
					data_declaration_type: int32_t
					data_declaration_name: acpi_proc_id
					data_declaration_end

					/* never use "cpu_role" name */
					/* never use "int" data type */
					data_declaration_type: int32_t
					data_declaration_name: role
					data_declaration_end
				struct_end

				struct_tag:
				struct_name:	funtions_s
				struct_start:
					/* structure pointer (with function pointers) declaration */
				struct_end

				struct_tag:
				struct_name:	signature_s
				struct_start:
					/* never use "cpu_signature" name */
					/* never use "int" data type */
					data_declaration_type: int32_t
					data_declaration_name: value
					data_declaration_end
				struct_end

				struct_tag:
				struct_name:	feature_s
				struct_start:
					/* never use "feature_flags" name */
					/* never use "int" data type */
					data_declaration_type: int32_t
					data_declaration_name: flags
					data_declaration_end
				struct_end
			struct_end

preprocessor_conditional:	{	! _KERNEL	}
preprocessor_conditional_start:
			/* functions declarations */
preprocessor_conditional_end

	algorithm_end

// code_end
