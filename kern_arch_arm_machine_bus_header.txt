// file_name:		arch/arm/include/sysbus.h	/* never use "arch/arm/include/bus.h" */
// file_name_end

// code_name:		System bus
// code_name_end

// required_headers:	#include <sys/types.h>		/* required by: int32_t */
// required_headers_end

// code:

	algorithm:	/* never use "#define" constant definition */
			/* never use "BUS_SPACE_BARRIER_READ" name and "0x01" value */
			def_constant_name:	SYSBUS_SP_BARRIER_R	def_constant_value:	000001	def_constant_end
						/* system bus space barrier read */
			/* never use "#define" constant definition */
			/* never use "BUS_SPACE_BARRIER_WRITE" name and "0x02" value */
			def_constant_name:	SYSBUS_SP_BARRIER_W	def_constant_value:	000002	def_constant_end
						/* system bus space barrier write */

			/* never use "#define" constant definition */
			/* never use "BUS_SPACE_MAP_CACHEABLE" name and "0x00001" value */
			def_constant_name:	SYSBUS_SP_MAP_CACHE	def_constant_value:	000001	def_constant_end
						/* system bus space map cacheable */
			/* never use "#define" constant definition */
			/* never use "BUS_SPACE_MAP_LINEAR" name and "0x00002" value */
			def_constant_name:	SYSBUS_SP_MAP_LIN	def_constant_value:	000002	def_constant_end
						/* system bus space map linear */
			/* never use "#define" constant definition */
			/* never use "BUS_SPACE_MAP_PREFETCHABLE" name and "0x00008" value */
			def_constant_name:	SYSBUS_SP_MAP_PR	def_constant_value:	000010	def_constant_end
						/* system bus space map prefetchable */

			/* never use "#define" constant definition */
			/* never use "BUS_DMA_WAITOK" name and "0x0000" value */
			def_constant_name:	SYSBUS_DMA_WAIT_OK	def_constant_value:	000000	def_constant_end
						/* system bus direct memory access wait ok */
			/* never use "#define" constant definition */
			/* never use "BUS_DMA_NOWAIT" name and "0x0001" value */
			def_constant_name:	SYSBUS_DMA_NO_WAIT	def_constant_value:	000001	def_constant_end
						/* system bus direct memory access no wait */
			/* never use "#define" constant definition */
			/* never use "BUS_DMA_ALLOCNOW" name and "0x0002" value */
			def_constant_name:	SYSBUS_DMA_ALLOC_NOW	def_constant_value:	000002	def_constant_end
						/* system bus direct memory access allocation now */
			/* never use "#define" constant definition */
			/* never use "BUS_DMA_COHERENT" name and "0x0004" value */
			def_constant_name:	SYSBUS_DMA_COHERENT	def_constant_value:	000004	def_constant_end
						/* system bus direct memory access coherent */
			/* never use "#define" constant definition */
			/* never use "BUS_DMA_STREAMING" name and "0x0008" value */
			def_constant_name:	SYSBUS_DMA_STREAMING	def_constant_value:	000010	def_constant_end
						/* system bus direct memory access streaming */
			/* never use "#define" constant definition */
			/* never use "BUS_DMA_BUS1" name and "0x0010" value */
			def_constant_name:	SYSBUS_DMA_BUS1		def_constant_value:	000020	def_constant_end
						/* system bus direct memory access bus 1 */
			/* never use "#define" constant definition */
			/* never use "BUS_DMA_BUS2" name and "0x0020" value */
			def_constant_name:	SYSBUS_DMA_BUS2		def_constant_value:	000040	def_constant_end
						/* system bus direct memory access bus 2 */
			/* never use "#define" constant definition */
			/* never use "BUS_DMA_BUS3" name and "0x0040" value */
			def_constant_name:	SYSBUS_DMA_BUS3		def_constant_value:	000100	def_constant_end
						/* system bus direct memory access bus 3 */
			/* never use "#define" constant definition */
			/* never use "BUS_DMA_BUS4" name and "0x0080" value */
			def_constant_name:	SYSBUS_DMA_BUS4		def_constant_value:	000200	def_constant_end
						/* system bus direct memory access bus 4 */
			/* never use "#define" constant definition */
			/* never use "BUS_DMA_READ" name and "0x0100" value */
			def_constant_name:	SYSBUS_DMA_READ		def_constant_value:	000400	def_constant_end
						/* system bus direct memory access read */
			/* never use "#define" constant definition */
			/* never use "BUS_DMA_WRITE" name and "0x0200" value */
			def_constant_name:	SYSBUS_DMA_WRITE	def_constant_value:	001000	def_constant_end
						/* system bus direct memory access write */
			/* never use "#define" constant definition */
			/* never use "BUS_DMA_NOCACHE" name and "0x0400" value */
			def_constant_name:	SYSBUS_DMA_NO_CACHE	def_constant_value:	002000	def_constant_end
						/* system bus direct memory access no cache */
			/* never use "#define" constant definition */
			/* never use "BUS_DMA_ZERO" name and "0x0800" value */
			def_constant_name:	SYSBUS_DMA_ZERO		def_constant_value:	004000	def_constant_end
						/* system bus direct memory access zero */
			/* never use "#define" constant definition */
			/* never use "BUS_DMA_64BIT" name and "0x1000" value */
			def_constant_name:	SYSBUS_DMA_DVA_64BITS	def_constant_value:	010000	def_constant_end
						/* system bus direct memory access 64bits (dva) */

			/* never use "#define" constant definition */
			/* never use "ARM32_DMAMAP_COHERENT" name and "0x10000" value */
			def_constant_name:	SYSBUS_DMAMAP_COHERENT	def_constant_value:	0200000	def_constant_end
						/* system bus direct memory access map coherent */

			/* never use "#define" constant definition */
			/* never use "BUS_DMASYNC_PREREAD" name and "0x01" value */
			def_constant_name:	SYSBUS_DMA_SYNC_PRE_R	def_constant_value:	000001	def_constant_end
						/* system bus direct memory access sync pre read */
			/* never use "#define" constant definition */
			/* never use "BUS_DMASYNC_POSTREAD" name and "0x02" value */
			def_constant_name:	SYSBUS_DMA_SYNC_POST_R	def_constant_value:	000002	def_constant_end
						/* system bus direct memory access sync post read */
			/* never use "#define" constant definition */
			/* never use "BUS_DMASYNC_PREWRITE" name and "0x04" value */
			def_constant_name:	SYSBUS_DMA_SYNC_PRE_W	def_constant_value:	000004	def_constant_end
						/* system bus direct memory access sync pre write */
			/* never use "#define" constant definition */
			/* never use "BUS_DMASYNC_POSTWRITE" name and "0x08" value */
			def_constant_name:	SYSBUS_DMA_SYNC_POST_W	def_constant_value:	000010	def_constant_end
						/* system bus direct memory access sync post write */

preprocessor_conditional:	{	_SYSBUS_DMA_PRIV	}	/* system bus direct memory access private */
preprocessor_conditional_start:
			/* never use "#define" constant definition */
			/* never use "ARM32_BUFTYPE_INVALID" name and "0" value */
			def_constant_name:	SYSBUS_BUFF_TYP_INV	def_constant_value:	000000	def_constant_end
						/* system bus buffer type invalid */
			/* never use "#define" constant definition */
			/* never use "ARM32_BUFTYPE_LINEAR" name and "1" value */
			def_constant_name:	SYSBUS_BUFF_TYP_LIN	def_constant_value:	000001	def_constant_end
						/* system bus buffer type linear */
			/* never use "#define" constant definition */
			/* never use "ARM32_BUFTYPE_MBUF" name and "2" value */
			def_constant_name:	SYSBUS_BUFF_TYP_MBUFF	def_constant_value:	000002	def_constant_end
						/* system bus buffer type mbuffer */
			/* never use "#define" constant definition */
			/* never use "ARM32_BUFTYPE_UIO" name and "3" value */
			def_constant_name:	SYSBUS_BUFF_TYP_UIO	def_constant_value:	000003	def_constant_end
						/* system bus buffer type uio */
			/* never use "#define" constant definition */
			/* never use "ARM32_BUFTYPE_RAW" name and "4" value */
			def_constant_name:	SYSBUS_BUFF_TYP_RAW	def_constant_value:	000004	def_constant_end
						/* system bus buffer type raw */
preprocessor_conditional_end

			/* never use "typedef" statement, structure name is not needed */
			/* never use "bus_addr_t" tag */
			struct_tag:	sysbus_address_t
			struct_name:
			struct_start:
				struct_tag:
				struct_name:	data_s
				struct_start:
					/* never use "u_long" data type */
					data_declaration_type:	unsigned long
					data_declaration_name:	value
					data_declaration_end
				struct_end
			struct_end

			/* never use "typedef" statement, structure name is not needed */
			/* never use "bus_size_t" tag */
			struct_tag:	sysbus_size_t
			struct_name:
			struct_start:
				struct_tag:
				struct_name:	data_s
				struct_start:
					/* never use "u_long" data type */
					data_declaration_type:	unsigned long
					data_declaration_name:	value
					data_declaration_end
				struct_end
			struct_end

			/* never use "typedef" statement, structure name is not needed */
			/* never use "bus_space_tag_t" tag */
			struct_tag:	sysbus_space_tag_ptr_t	/* "system_bus_space_tag_pointer_t"
			struct_name:
			struct_start:
				struct_tag:
				struct_name:	data_s
				struct_start:
					/* it is a redeclaration of "sysbus_space_t" structure */
					struct_tag:	<redeclaration of: sysbus_space_t>
					struct_name:	pointer_s *
					struct_start_and_end
				struct_end
			struct_end

			/* never use "typedef" statement, structure name is not needed */
			/* never use "bus_space_handle_t" tag */
			struct_tag:	sysbus_space_handle_t
			struct_name:
			struct_start:
				struct_tag:
				struct_name:	data_s
				struct_start:
					/* never use "u_long" data type */
					data_declaration_type:	unsigned long
					data_declaration_name:	value
					data_declaration_end
				struct_end
			struct_end

			/* never use "bus_space" tag */
			struct_tag:	sysbus_space_t
			struct_name:
			struct_start:
				/* never use "bs_cookie" name */
				struct_tag:
				struct_name:	cookie_s
				struct_start:
					/* it is a VOID pointer */
					data_declaration_type:	void *
					data_declaration_name:	pointer
					data_declaration_end
				struct_end

				struct_tag:
				struct_name:	operations_s
				struct_start:
					struct_tag:
					struct_name:	mapping_s
					struct_start:
						struct_tag:
						struct_name:	data_s
						struct_start:
							/* function pointer declaration */
							/* function pointer declaration */
							/* function pointer declaration */
						struct_end
					struct_end

					struct_tag:
					struct_name:	allocation_s
					struct_start:
						struct_tag:
						struct_name:	data_s
						struct_start:
							/* function pointer declaration */
							/* function pointer declaration */
						struct_end
					struct_end

					struct_tag:
					struct_name:	virtual_s
					struct_start:
						struct_tag:
						struct_name:	address_s
						struct_start:
							/* function pointer declaration */
						struct_end
					struct_end

					struct_tag:
					struct_name:	mmap_s
					struct_start:
						struct_tag:
						struct_name:	data_s
						struct_start:
							/* function pointer declaration */
						struct_end
					struct_end

					struct_tag:
					struct_name:	barrier_s
					struct_start:
						struct_tag:
						struct_name:	data_s
						struct_start:
							/* function pointer declaration */
						struct_end
					struct_end

					struct_tag:
					struct_name:	read_s
					struct_start:
						struct_tag:
						struct_name:	single_s
						struct_start:
							/* function pointer declaration */
							/* function pointer declaration */
							/* function pointer declaration */
							/* function pointer declaration */
						struct_end

						struct_tag:
						struct_name:	multiple_s
						struct_start:
							/* function pointer declaration */
							/* function pointer declaration */
							/* function pointer declaration */
							/* function pointer declaration */
						struct_end

						struct_tag:
						struct_name:	region_s
						struct_start:
							/* function pointer declaration */
							/* function pointer declaration */
							/* function pointer declaration */
							/* function pointer declaration */
						struct_end
					struct_end

					struct_tag:
					struct_name:	write_s
					struct_start:
						struct_tag:
						struct_name:	single_s
						struct_start:
							/* function pointer declaration */
							/* function pointer declaration */
							/* function pointer declaration */
							/* function pointer declaration */
						struct_end

						struct_tag:
						struct_name:	multiple_s
						struct_start:
							/* function pointer declaration */
							/* function pointer declaration */
							/* function pointer declaration */
							/* function pointer declaration */
						struct_end

						struct_tag:
						struct_name:	region_s
						struct_start:
							/* function pointer declaration */
							/* function pointer declaration */
							/* function pointer declaration */
							/* function pointer declaration */
						struct_end
					struct_end

					struct_tag:
					struct_name:	set_s
					struct_start:
						struct_tag:
						struct_name:	multiple_s
						struct_start:
							/* function pointer declaration */
							/* function pointer declaration */
							/* function pointer declaration */
							/* function pointer declaration */
						struct_end

						struct_tag:
						struct_name:	region_s
						struct_start:
							/* function pointer declaration */
							/* function pointer declaration */
							/* function pointer declaration */
							/* function pointer declaration */
						struct_end
					struct_end

					struct_tag:
					struct_name:	copy_s
					struct_start:
						struct_tag:
						struct_name:	data_s
						struct_start:
							/* function pointer declaration */
							/* function pointer declaration */
							/* function pointer declaration */
							/* function pointer declaration */
						struct_end
					struct_end
				struct_end
			struct_end

			/* never use "typedef" statement, structure name is not needed */
			/* never use "bus_dma_tag_t" tag */
			struct_tag:	sysbus_dma_tag_ptr_t	/* "system_bus_direct_memory_access_tag_pointer_t" */
			struct_name:
			struct_start:
				struct_tag:
				struct_name:	data_s
				struct_start:
					/* it is a redeclaration of "sysbus_dma_tag_t" structure */
					struct_tag:	<redeclaration of: sysbus_dma_tag_t>
					struct_name:	pointer_s *
					struct_start_and_end
				struct_end
			struct_end

			/* never use "typedef" statement, structure name is not needed */
			/* never use "bus_dmamap_t" tag */
			struct_tag:	sysbus_dma_map_ptr_t	/* "system_bus_direct_memory_access_map_pointer_t" */
			struct_name:
			struct_start:
				struct_tag:
				struct_name:	data_s
				struct_start:
					/* it is a redeclaration of "sysbus_dma_map_t" structure */
					struct_tag:	<redeclaration of: sysbus_dma_map_t>
					struct_name:	pointer_s *
					struct_start_and_end
				struct_end
			struct_end

			/* never use "bus_dma_segment_t" typedef */
			/* never use "arm32_bus_dma_segment" tag */
			struct_tag:	sysbus_dma_segment_t	/* "system_bus_direct_memory_access_segment_t" */
			struct_name:
			struct_start:
				/* never use "ds_addr" name */
				struct_tag:
				struct_name:	address_s
				struct_start:
					/* it is a redeclaration of "sysbus_address_t" structure */
					struct_tag:	<redeclaration of: sysbus_address_t>
					struct_name:	data_s
					struct_start_and_end
				struct_end

				struct_tag:
				struct_name:	size_s
				struct_start:
					/* never use "ds_len" name */
					/* it is a redeclaration of "sysbus_size_t" structure */
					struct_tag:	<redeclaration of: sysbus_size_t>
					struct_name:	length_s
					struct_start_and_end
				struct_end

				struct_tag:
				struct_name:	_virtual_s
				struct_start:
					/* never use "_ds_vaddr" name */
					/* it is a redeclaration of "sysbus_address_t" structure */
					struct_tag:	<redeclaration of: sysbus_address_t>
					struct_name:	address_s
					struct_start_and_end
				struct_end

				struct_tag:
				struct_name:	_coherent_s
				struct_start:
					/* never use "int" data type */
					data_declaration_type:	int32_t
					data_declaration_name:	value
					data_declaration_end
				struct_end
			struct_end

			/* never use "arm32_dma_range" tag */
			struct_tag:	sysbus_dma_range_t
			struct_name:
			struct_start:
				struct_tag:
				struct_name:	system_s
				struct_start:
					/* it is a redeclaration of "sysbus_address_t" structure */
					struct_tag:	<redeclaration of: sysbus_address_t>
					struct_name:	base_s
					struct_start_and_end
				struct_end

				struct_tag:
				struct_name:	bus_s
				struct_start:
					/* it is a redeclaration of "sysbus_address_t" structure */
					struct_tag:	<redeclaration of: sysbus_address_t>
					struct_name:	base_s
					struct_start_and_end
				struct_end

				struct_tag:
				struct_name:	data_s
				struct_start:
					/* it is a redeclaration of "sysbus_size_t" structure */
					struct_tag:	<redeclaration of: sysbus_size_t>
					struct_name:	length_s
					struct_start_and_end
				struct_end
			struct_end

			/* never use "arm32_bus_dma_tag" tag */
			struct_tag:	sysbus_dma_tag_t	/* "system_bus_direct_memory_access_tag_t" */
			struct_name:
			struct_start:
				struct_tag:
				struct_name:	_ranges_s
				struct_start:
					/* never use "_ranges" name */
					/* it is a redeclaration of "sysbus_dma_range_t" structure */
					struct_tag:	<redeclaration of: sysbus_dma_range_t>
					struct_name:	pointer_s *
					struct_start_and_end

					/* never use "_nranges" name */
					/* never use "int" data type */
					data_declaration_type:	int32_t
					data_declaration_name:	count
					data_declaration_end
				struct_end

				struct_tag:
				struct_name:	_cookie_s
				struct_start:
					/* it is a VOID pointer */
					data_declaration_type:	void *
					data_declaration_name:	pointer
					data_declaration_end
				struct_end

				struct_tag:
				struct_name:	functions_s
				struct_start:
					struct_tag:
					struct_name:	mapping_s
					struct_start:
						/* function pointer declaration */
						/* function pointer declaration */
						/* function pointer declaration */
						/* function pointer declaration */
						/* function pointer declaration */
						/* function pointer declaration */
						/* function pointer declaration */
						/* function pointer declaration */
					struct_end

					struct_tag:
					struct_name:	memory_s
					struct_start:
						/* function pointer declaration */
						/* function pointer declaration */
						/* function pointer declaration */
						/* function pointer declaration */
						/* function pointer declaration */
					struct_end
				struct_end
			struct_end

			/* never use "arm32_bus_dmamap" tag */
			struct_tag:	sysbus_dma_map_t	/* "system_bus_direct_memory_access_map_t" */
			struct_name:
			struct_start:
				struct_tag:
				struct_name:	_data_s
				struct_start:
					/* never use "_dm_size" name */
					/* it is a redeclaration of "sysbus_size_t" structure */
					struct_tag:	<redeclaration of: sysbus_size_t>
					struct_name:	size_s
					struct_start_and_end
				struct_end

				struct_tag:
				struct_name:	_segment_s
				struct_start:
					/* never use "_dm_segcnt" name */
					/* never use "int" data type */
					data_declaration_type:	int32_t
					data_declaration_name:	count
					data_declaration_end
				struct_end

				struct_tag:
				struct_name:	_size_s
				struct_start:
					/* never use "_dm_maxsegsz" name */
					/* it is a redeclaration of "sysbus_size_t" structure */
					struct_tag:	<redeclaration of: sysbus_size_t>
					struct_name:	max_segment_s
					struct_start_and_end

					/* never use "_dm_boundary" name */
					/* it is a redeclaration of "sysbus_size_t" structure */
					struct_tag:	<redeclaration of: sysbus_size_t>
					struct_name:	boundary_s
					struct_start_and_end
				struct_end

				struct_tag:
				struct_name:	_misc_s
				struct_start:
					/* never use "_dm_flags" name */
					/* never use "int" data type */
					data_declaration_type:	int32_t
					data_declaration_name:	flags
					data_declaration_end
				struct_end

				struct_tag:
				struct_name:	_buffer_s
				struct_start:
					/* never use "_dm_origbuf" name */
					/* it is a VOID pointer */
					data_declaration_type:	void *
					data_declaration_name:	original
					data_declaration_end

					/* never use "_dm_buftype" name */
					/* never use "int" data type */
					data_declaration_type:	int32_t
					data_declaration_name:	type
					data_declaration_end
				struct_end

				struct_tag:
				struct_name:	_process_s
				struct_start:
					/* never use "_dm_proc" name */
					/* it is a pointer *
					/* it is a redeclaration of "proc" structure */
					struct_tag:	<redeclaration of: proc>
					struct_name:	pointer_s *
					struct_start_and_end
				struct_end

				struct_tag:
				struct_name:	_cookie_s
				struct_start:
					/* never use "_dm_segcnt" name */
					/* it is a VOID pointer */
					data_declaration_type:	void *
					data_declaration_name:	count
					data_declaration_end
				struct_end

				struct_tag:
				struct_name:	map_s
				struct_start:
					/* never use "dm_mapsize" name */
					/* it is a redeclaration of "sysbus_size_t" structure */
					struct_tag:	<redeclaration of: sysbus_size_t>
					struct_name:	size_s
					struct_start_and_end
				struct_end

				struct_tag:
				struct_name:	segments_s
				struct_start:
					/* never use "dm_nsegs" name */
					/* never use "int" data type */
					data_declaration_type:	int32_t
					data_declaration_name:	number
					data_declaration_end

					/* never use "dm_segs" name */
					/* it is an array */
					/* it is a redeclaration of "sysbus_dma_segment_t" structure */
					struct_tag:	<redeclaration of: sysbus_dma_segment_t>
					struct_name:	length_s[1]
					struct_start_and_end
				struct_end
			struct_end

			/* functions definitions with asm */

preprocessor_conditional:	{	_SYSBUS_DMA_PRIV	}	/* system bus direct memory access private */
preprocessor_conditional_start:
			/* functions declarations */
preprocessor_conditional_end

	algorithm_end

// code_end
