// file_name:		arch/arm/include/pgtable.h	/* never use "arch/arm/include/pte.h" */
// file_name_end

// code_name:		Page Table
// code_name_end

// required_headers:	#include <sys/types.h>		/* required by: u_int and int32_t */
// required_headers_end

// code:

	algorithm:

preprocessor_conditional:	{	! _LOCORE	}
preprocessor_conditional_start:
			/* never use "pd_entry_t" name */
			/* never use "uint32_t" data type */
			type_def_name:	pgdir_t		type_def_value:	u_int	type_def_end
			/* never use "pt_entry_t" name */
			/* never use "uint32_t" data type */
			type_def_name:	pgtable_t	type_def_value:	u_int	type_def_end
preprocessor_conditional_end

			/* never use "#define" constant definition */
			/* never use "L1_S_SIZE" name and "0x00100000" value */
			def_constant_name:	PGTABLE_LVL1_SECT_SIZE	def_constant_value:	04000000			def_constant_end
						/* page table -entry- level 1 section size */
			/* never use "#define" constant definition */
			/* never use "L1_S_OFFSET" name and "L1_S_SIZE - 1" value */
			def_constant_name:	PGTABLE_LVL1_SECT_OFF	def_constant_value:	(- 1) + PGTABLE_LVL1_SECT_SIZE	def_constant_end
						/* page table -entry- level 1 section offset */
			/* never use "#define" constant definition */
			/* never use "L1_S_FRAME" name and "~L1_S_OFFSET" value */
			def_constant_name:	PGTABLE_LVL1_SECT_FRM	def_constant_value:	~ PGTABLE_LVL1_SECT_OFF		def_constant_end
						/* page table -entry- level 1 section frame */
			/* never use "#define" constant definition */
			/* never use "L1_S_SHIFT" name and "20" value */
			def_constant_name:	PGTABLE_LVL1_SECT_SHFT	def_constant_value:	00000024			def_constant_end
						/* page table -entry- level 1 section shift */

			/* never use "#define" constant definition */
			/* never use "L2_L_SIZE" name and "0x00010000" value */
			def_constant_name:	PGTABLE_LVL2_LGPG_SIZE	def_constant_value:	00200000			def_constant_end
						/* page table -entry- level 2 large page size */
			/* never use "#define" constant definition */
			/* never use "L2_L_OFFSET" name and "L2_L_SIZE - 1" value */
			def_constant_name:	PGTABLE_LVL2_LGPG_OFF	def_constant_value:	(- 1) + PGTABLE_LVL2_LGPG_SIZE	def_constant_end
						/* page table -entry- level 2 large page offset */
			/* never use "#define" constant definition */
			/* never use "L2_L_FRAME" name and "~L2_L_OFFSET" value */
			def_constant_name:	PGTABLE_LVL2_LGPG_FRM	def_constant_value:	~ PGTABLE_LVL2_LGPG_OFF		def_constant_end
						/* page table -entry- level 2 large page frame */
			/* never use "#define" constant definition */
			/* never use "L2_L_SHIFT" name and "16" value */
			def_constant_name:	PGTABLE_LVL2_LGPG_SHFT	def_constant_value:	00000020			def_constant_end
						/* page table -entry- level 2 large page shift */

			/* never use "#define" constant definition */
			/* never use "L2_S_SIZE" name and "0x00001000" value */
			def_constant_name:	PGTABLE_LVL2_SMPG_SIZE	def_constant_value:	00010000			def_constant_end
						/* page table -entry- level 2 small page size */
			/* never use "#define" constant definition */
			/* never use "L2_S_OFFSET" name and "L2_S_SIZE - 1" value */
			def_constant_name:	PGTABLE_LVL2_SMPG_OFF	def_constant_value:	(- 1) + PGTABLE_LVL2_SMPG_SIZE	def_constant_end
						/* page table -entry- level 2 small page offset */
			/* never use "#define" constant definition */
			/* never use "L2_S_FRAME" name and "~L2_S_OFFSET" value */
			def_constant_name:	PGTABLE_LVL2_SMPG_FRM	def_constant_value:	~ PGTABLE_LVL2_SMPG_OFF		def_constant_end
						/* page table -entry- level 2 small page frame */
			/* never use "#define" constant definition */
			/* never use "L2_S_SHIFT" name and "12" value */
			def_constant_name:	PGTABLE_LVL2_SMPG_SHFT	def_constant_value:	00000014			def_constant_end
						/* page table -entry- level 2 small page shift */

			/* never use "#define" constant definition */
			/* never use "L2_T_SIZE" name and "0x00000400" value */
			def_constant_name:	PGTABLE_LVL2_TNPG_SIZE	def_constant_value:	00002000			def_constant_end
						/* page table -entry- level 2 tiny page size */
			/* never use "#define" constant definition */
			/* never use "L2_T_OFFSET" name and "L2_T_SIZE - 1" value */
			def_constant_name:	PGTABLE_LVL2_TNPG_OFF	def_constant_value:	(- 1) + PGTABLE_LVL2_TNPG_SIZE	def_constant_end
						/* page table -entry- level 2 tiny page offset */
			/* never use "#define" constant definition */
			/* never use "L2_T_FRAME" name and "~L2_T_OFFSET" value */
			def_constant_name:	PGTABLE_LVL2_TNPG_FRM	def_constant_value:	~ PGTABLEE_LVL2_TNPG_OFF	def_constant_end
						/* page table -entry- level 2 tiny page frame */
			/* never use "#define" constant definition */
			/* never use "L2_T_SHIFT" name and "10" value */
			def_constant_name:	PGTABLE_LVL2_TNPG_SHFT	def_constant_value:	00000012			def_constant_end
						/* page table -entry- level 2 tiny page shift */

			/* never use "#define" constant definition */
			/* never use "L1_ADDR_BITS" name and "0xfff00000" value */
			def_constant_name:	PGTABLE_LVL1_ADDR_BITS	def_constant_value:	037774000000			def_constant_end
						/* page table -entry- level 1 address bits */
			/* never use "#define" constant definition */
			/* never use "L2_ADDR_BITS" name and "0x000ff000" value */
			def_constant_name:	PGTABLE_LVL2_ADDR_BITS	def_constant_value:	000003770000			def_constant_end
						/* page table -entry- level 2 address bits */

			/* never use "#define" constant definition */
			/* never use "L1_TABLE_SIZE" name and "0x4000" value */
			def_constant_name:	PGTABLE_LVL1_TBL_SIZ	def_constant_value:	040000				def_constant_end
						/* page table -entry- level 1 table size */
			/* never use "#define" constant definition */
			/* never use "L2_TABLE_SIZE" name and "0x1000" value */
			def_constant_name:	PGTABLE_LVL2_TBL_SIZ	def_constant_value:	010000				def_constant_end
						/* page table -entry- level 2 table size */

			/* never use "#define" constant definition */
			/* never use "L2_TABLE_SIZE_REAL" name and "0x400" value */
			def_constant_name:	PGTABLE_LVL2_TBL_SIZ_R	def_constant_value:	002000				def_constant_end
						/* page table -entry- level 2 table size real */

			/* never use "#define" constant definition */
			/* never use "L1_TYPE_INV" name and "0x00" value */
			def_constant_name:	PGTABLE_LVL1_TP_INV	def_constant_value:	00				def_constant_end
						/* page table -entry- level 1 type invalid */
			/* never use "#define" constant definition */
			/* never use "L1_TYPE_C" name and "0x01" value */
			def_constant_name:	PGTABLE_LVL1_TP_CRS	def_constant_value:	01				def_constant_end
						/* page table -entry- level 1 type coarse */
			/* never use "#define" constant definition */
			/* never use "L1_TYPE_S" name and "0x02" value */
			def_constant_name:	PGTABLE_LVL1_TP_SECT	def_constant_value:	02				def_constant_end
						/* page table -entry- level 1 type section */
			/* never use "#define" constant definition */
			/* never use "L1_TYPE_F" name and "0x03" value */
			def_constant_name:	PGTABLE_LVL1_TP_FINE	def_constant_value:	03				def_constant_end
						/* page table -entry- level 1 type fine */
			/* never use "#define" constant definition */
			/* never use "L1_TYPE_MASK" name and "0x03" value */
			def_constant_name:	PGTABLE_LVL1_TP_MASK	def_constant_value:	03				def_constant_end
						/* page table -entry- level 1 type mask */

			/* never use "#define" constant definition */
			/* never use "L1_S_B" name and "0x00000004" value */
			def_constant_name:	PGTABLE_LVL1_SECT_BUFF	def_constant_value:	000000000004			def_constant_end
						/* page table -entry- level 1 section buffeable */
			/* never use "#define" constant definition */
			/* never use "L1_S_C" name and "0x00000008" value */
			def_constant_name:	PGTABLE_LVL1_SECT_CACH	def_constant_value:	000000000010			def_constant_end
						/* page table -entry- level 1 section cacheable */
			/* never use "#define" constant definition */
			/* never use "L1_S_IMP" name and "0x00000010" value */
			def_constant_name:	PGTABLE_LVL1_SECT_IDEF	def_constant_value:	000000000012			def_constant_end
						/* page table -entry- level 1 section implementation defined */
			/* never use "#define" constant definition */
			/* never use "L1_S_DOM_MASK" name and "L1_S_DOM(0xf)" value */
			def_constant_name:	PGTABLE_LVL1_SECT_DMSK	def_constant_value:	000000000017 << 000000000005	def_constant_end
						/* page table -entry- level 1 section domain mask */
			/* never use "#define" constant definition */
			/* never use "L1_S_ADDR_MASK" name and "0xfff00000" value */
			def_constant_name:	PGTABLE_LVL1_SECT_PAM	def_constant_value:	037774000000			def_constant_end
						/* page table -entry- level 1 section physical address mask */

			/* never use "#define" constant definition */
			/* never use "L1_S_V7_TEX_MASK" name and "0x7 << 12" value */
			def_constant_name:	PGTABLE_LVL1_SEC7_TXM	def_constant_value:	00000007 << 00000014		def_constant_end
						/* page table -entry- level 1 section armv7 type extension mask */
			/* never use "#define" constant definition */
			/* never use "L1_S_V7_NS" name and "0x00080000" value */
			def_constant_name:	PGTABLE_LVL1_SEC7_NONS	def_constant_value:	02000000			def_constant_end
						/* page table -entry- level 1 section armv7 non secure */
			/* never use "#define" constant definition */
			/* never use "L1_S_V7_SS" name and "0x00040000" value */
			def_constant_name:	PGTABLE_LVL1_SEC7_SSEC	def_constant_value:	01000000			def_constant_end
						/* page table -entry- level 1 section armv7 super section */
			/* never use "#define" constant definition */
			/* never use "L1_S_V7_nG" name and "0x00020000" value */
			def_constant_name:	PGTABLE_LVL1_SEC7_NOTG	def_constant_value:	00400000			def_constant_end
						/* page table -entry- level 1 section armv7 not global */
			/* never use "#define" constant definition */
			/* never use "L1_S_V7_S" name and "0x00010000" value */
			def_constant_name:	PGTABLE_LVL1_SEC7_SHA	def_constant_value:	00200000			def_constant_end
						/* page table -entry- level 1 section armv7 shareable */
			/* never use "#define" constant definition */
			/* never use "L1_S_V7_AF" name and "0x00000400" value */
			def_constant_name:	PGTABLE_LVL1_SEC7_AFLG	def_constant_value:	00002000			def_constant_end
						/* page table -entry- level 1 section armv7 access flag */
			/* never use "#define" constant definition */
			/* never use "L1_S_V7_IMP" name and "0x00000200" value */
			def_constant_name:	PGTABLE_LVL1_SEC7_IDEF	def_constant_value:	00001000			def_constant_end
						/* page table -entry- level 1 section armv7 implementation defined */
			/* never use "#define" constant definition */
			/* never use "L1_S_V7_XN" name and "0x00000010" value */
			def_constant_name:	PGTABLE_LVL1_SEC7_XN	def_constant_value:	00000020			def_constant_end
						/* page table -entry- level 1 section armv7 execute never */
			/* never use "#define" constant definition */
			/* never use "L1_S_V7_PXN" name and "0x00000001" value */
			def_constant_name:	PGTABLE_LVL1_SEC7_PXN	def_constant_value:	00000001			def_constant_end
						/* page table -entry- level 1 section armv7 privileged execute never */

			/* never use "#define" constant definition */
			/* never use "L1_C_IMP0" name and "0x00000004" value */
			def_constant_name:	PGTABLE_LVL1_CRS_I0D	def_constant_value:	000000000004			def_constant_end
						/* page table -entry- level 1 coarse implementation 0 defined */
			/* never use "#define" constant definition */
			/* never use "L1_C_IMP1" name and "0x00000008" value */
			def_constant_name:	PGTABLE_LVL1_CRS_I1D	def_constant_value:	000000000010			def_constant_end
						/* page table -entry- level 1 coarse implementation 1 defined */
			/* never use "#define" constant definition */
			/* never use "L1_C_IMP2" name and "0x00000010" value */
			def_constant_name:	PGTABLE_LVL1_CRS_I2D	def_constant_value:	000000000012			def_constant_end
						/* page table -entry- level 1 coarse implementation 2 defined */
			/* never use "#define" constant definition */
			/* never use "L1_C_DOM_MASK" name and "L1_C_DOM(0xf)" value */
			def_constant_name:	PGTABLE_LVL1_CRS_DMSK	def_constant_value:	000000000017 << 000000000005	def_constant_end
						/* page table -entry- level 1 coarse domain mask */
			/* never use "#define" constant definition */
			/* never use "L1_C_ADDR_MASK" name and "0xfffffc00" value */
			def_constant_name:	PGTABLE_LVL1_CRS_PAM	def_constant_value:	037777776000			def_constant_end
						/* page table -entry- level 1 coarse physical address mask */

			/* never use "#define" constant definition */
			/* never use "L1_C_V7_IMP" name and "0x00000200" value */
			def_constant_name:	PGTABLE_LVL1_CRS7_IDEF	def_constant_value:	00001000			def_constant_end
						/* page table -entry- level 1 coarse armv7 implementation defined */
			/* never use "#define" constant definition */
			/* never use "L1_C_V7_NS" name and "0x00000008" value */
			def_constant_name:	PGTABLE_LVL1_CRS7_NONS	def_constant_value:	00000010			def_constant_end
						/* page table -entry- level 1 coarse armv7 non secure */
			/* never use "#define" constant definition */
			/* never use "L1_C_V7_PXN" name and "0x00000004" value */
			def_constant_name:	PGTABLE_LVL1_CRS7_PXN	def_constant_value:	00000001			def_constant_end
						/* page table -entry- level 1 coarse armv7 privileged execute never */

			/* never use "#define" constant definition */
			/* never use "L1_F_IMP0" name and "0x00000004" value */
			def_constant_name:	PGTABLE_LVL1_FINE_I0D	def_constant_value:	000000000004			def_constant_end
						/* page table -entry- level 1 fine implementation 0 defined */
			/* never use "#define" constant definition */
			/* never use "L1_F_IMP1" name and "0x00000008" value */
			def_constant_name:	PGTABLE_LVL1_FINE_I1D	def_constant_value:	000000000010			def_constant_end
						/* page table -entry- level 1 fine implementation 1 defined */
			/* never use "#define" constant definition */
			/* never use "L1_F_IMP2" name and "0x00000010" value */
			def_constant_name:	PGTABLE_LVL1_FINE_I2D	def_constant_value:	000000000012			def_constant_end
						/* page table -entry- level 1 fine implementation 2 defined */
			/* never use "#define" constant definition */
			/* never use "L1_F_DOM_MASK" name and "L1_F_DOM(0xf)" value */
			def_constant_name:	PGTABLE_LVL1_FINE_DMSK	def_constant_value:	000000000017 << 000000000005	def_constant_end
						/* page table -entry- level 1 fine domain mask */
			/* never use "#define" constant definition */
			/* never use "L1_F_ADDR_MASK" name and "0xfffff000" value */
			def_constant_name:	PGTABLE_LVL1_FINE_PAM	def_constant_value:	037777770000			def_constant_end
						/* page table -entry- level 1 fine physical address mask */

			/* never use "#define" constant definition */
			/* never use "L2_TYPE_INV" name and "0x00" value */
			def_constant_name:	PGTABLE_LVL2_TP_INV	def_constant_value:	00				def_constant_end
						/* page table -entry- level 2 type invalid */
			/* never use "#define" constant definition */
			/* never use "L2_TYPE_L" name and "0x01" value */
			def_constant_name:	PGTABLE_LVL2_TP_LGPG	def_constant_value:	01				def_constant_end
						/* page table -entry- level 2 type large page */
			/* never use "#define" constant definition */
			/* never use "L2_TYPE_S" name and "0x02" value */
			def_constant_name:	PGTABLE_LVL2_TP_SMPG	def_constant_value:	02				def_constant_end
						/* page table -entry- level 2 type small page */
			/* never use "#define" constant definition */
			/* never use "L2_TYPE_T" name and "0x03" value */
			def_constant_name:	PGTABLE_LVL2_TP_TNPG	def_constant_value:	03				def_constant_end
						/* page table -entry- level 2 type tiny page */
			/* never use "#define" constant definition */
			/* never use "L2_TYPE_MASK" name and "0x03" value */
			def_constant_name:	PGTABLE_LVL2_TP_MASK	def_constant_value:	03				def_constant_end
						/* page table -entry- level 2 type mask */

			/* never use "#define" constant definition */
			/* never use "L2_B" name and "0x00000004" value */
			def_constant_name:	PGTABLE_LVL2_PG_BUFF	def_constant_value:	000000000004			def_constant_end
						/* page table -entry- level 2 page buffeable */
			/* never use "#define" constant definition */
			/* never use "L2_C" name and "0x00000008" value */
			def_constant_name:	PGTABLE_LVL2_PG_CACH	def_constant_value:	000000000010			def_constant_end
						/* page table -entry- level 2 page cacheable */

			/* never use "#define" constant definition */
			/* never use "L2_V7_L_TEX_MASK" name and "0x7 << 12" value */
			def_constant_name:	PGTABLE_LVL2_LGP7_TXM	def_constant_value:	00000007 << 00000014		def_constant_end
						/* page table -entry- level 2 large page armv7 type extension mask */
			/* never use "#define" constant definition */
			/* never use "L2_V7_L_XN" name and "0x00008000" value */
			def_constant_name:	PGTABLE_LVL2_LGP7_XN	def_constant_value:	00100000			def_constant_end
						/* page table -entry- level 2 large page armv7 execute never */
			/* never use "#define" constant definition */
			/* never use "L2_V7_S_TEX_MASK" name and "0x7 << 6" value */
			def_constant_name:	PGTABLE_LVL2_SMP7_TXM	def_constant_value:	00000007 << 00000006		def_constant_end
						/* page table -entry- level 2 small page armv7 type extension mask */
			/* never use "#define" constant definition */
			/* never use "L2_V7_S_XN" name and "0x00000001" value */
			def_constant_name:	PGTABLE_LVL2_SMP7_XN	def_constant_value:	00000001			def_constant_end
						/* page table -entry- level 2 small page armv7 execute never */

			/* never use "#define" constant definition */
			/* never use "L2_V7_AF" name and "0x00000010" value */
			def_constant_name:	PGTABLE_LVL2_P7_AFLG	def_constant_value:	00020				def_constant_end
						/* page table -entry- level 2 page armv7 access flag */
			/* never use "#define" constant definition */
			/* never use "L2_V7_S" name and "0x00000400" value */
			def_constant_name:	PGTABLE_LVL2_P7_SHA	def_constant_value:	02000				def_constant_end
						/* page table -entry- level 2 page armv7 shareable */
			/* never use "#define" constant definition */
			/* never use "L2_V7_nG" name and "0x00000800" value */
			def_constant_name:	PGTABLE_LVL2_P7_NOTG	def_constant_value:	04000				def_constant_end
						/* page table -entry- level 2 page armv7 not global */

			/* never use "#define" constant definition */
			/* never use "AP_KR" name and "0x00" value */
			def_constant_name:	PGTABLE_APERM_KRO	def_constant_value:	00				def_constant_end
						/* page table -entry- access permissions-;- kernel-space- read only */
			/* never use "#define" constant definition */
			/* never use "AP_V7_KR" name and "0x05" value */
			def_constant_name:	PGTABLE_APER7_KRO	def_constant_value:	05				def_constant_end
						/* page table -entry- access permissions armv7-;- kernel-space- read only */
			/* never use "#define" constant definition */
			/* never use "AP_KRW" name and "0x01" value */
			def_constant_name:	PGTABLE_APERM_KRW	def_constant_value:	01				def_constant_end
						/* page table -entry- access permissions-;- kernel-space- read -and- write */
			/* never use "#define" constant definition */
			/* never use "AP_KRWUR" name and "0x02" value */
			def_constant_name:	PGTABLE_APERM_KRW_URO	def_constant_value:	02				def_constant_end
						/* page table -entry- access permissions-;- kernel-space- read -and- write-;- -and- user-space- read only */
			/* never use "#define" constant definition */
			/* never use "AP_V7_KRUR" name and "0x07" value */
			def_constant_name:	PGTABLE_APER7_KRO_URO	def_constant_value:	07				def_constant_end
						/* page table -entry- access permissions armv7-;- kernel-space- read only-;- -and- user-space- read only */
			/* never use "#define" constant definition */
			/* never use "AP_KRWURW" name and "0x03" value */
			def_constant_name:	PGTABLE_APERM_KRW_URW	def_constant_value:	03				def_constant_end
						/* page table -entry- access permissions-;- kernel-space- read -and- write-;- -and- user-space- read -and- write */

			/* never use "#define" constant definition */
			/* never use "DOMAIN_FAULT" name and "0x00" value */
			def_constant_name:	PGTABLE_DACTL_FAULT	def_constant_value:	00				def_constant_end
						/* page table -entry- domain access control fault */
			/* never use "#define" constant definition */
			/* never use "DOMAIN_CLIENT" name and "0x01" value */
			def_constant_name:	PGTABLE_DACTL_CLIENT	def_constant_value:	01				def_constant_end
						/* page table -entry- domain access control client */
			/* never use "#define" constant definition */
			/* never use "DOMAIN_RESERVED" name and "0x02" value */
			def_constant_name:	PGTABLE_DACTL_RESERVED	def_constant_value:	02				def_constant_end
						/* page table -entry- domain access control reserved */
			/* never use "#define" constant definition */
			/* never use "DOMAIN_MANAGER" name and "0x03" value */
			def_constant_name:	PGTABLE_DACTL_MANAGER	def_constant_value:	03				def_constant_end
						/* page table -entry- domain access control manager */

			/* never use "#define" constant definition */
			/* never use "x" variable parameter, use "value" variable parameter and "int32_t" data type instead */
			LOCAL 1)	pagetable_level1_any_domain			/* never use "L1_S_DOM", "L1_C_DOM" and "L1_F_DOM" name */

			algorithm:	/* never use "x << 5" */
					return value << 005		/* return with "int32_t" data type */
			algorithm_end

			/* never use "#define" constant definition */
			/* never use "x" variable parameter, use "value" variable parameter and "int32_t" data type instead */
			LOCAL 2)	pagetable_level1_section_accesspermitions	/* never use "L1_S_AP" and "L1_S_V7_AP" name */

			algorithm:	/* never use "x << 10" */
					return value << 012		/* return with "int32_t" data type */
			algorithm_end

			/* never use "#define" constant definition */
			/* never use "x" variable parameter, use "value" variable parameter and "int32_t" data type instead */
			LOCAL 3)	pagetable_level1or2_any_armv7_typeextension	/* never use "L1_S_V7_TEX" and "L2_V7_L_TEX" name */

			algorithm:	/* never use "(x & 0x7) << 12" */
					return (007 & value) << 014	/* return with "int32_t" data type */
			algorithm_end

			/* never use "#define" constant definition */
			/* never use "x" variable parameter, use "value" variable parameter and "int32_t" data type instead */
			LOCAL 4)	pagetable_level2_page_accesspermitions0		/* never use "L2_AP0" name */

			algorithm:	/* never use "x << 4" */
					return value << 004		/* return with "int32_t" data type */
			algorithm_end

			/* never use "#define" constant definition */
			/* never use "x" variable parameter, use "value" variable parameter and "int32_t" data type instead */
			LOCAL 5)	pagetable_level2_page_accesspermitions1		/* never use "L2_AP1" name */

			algorithm:	/* never use "x << 6" */
					return value << 006		/* return with "int32_t" data type */
			algorithm_end

			/* never use "#define" constant definition */
			/* never use "x" variable parameter, use "value" variable parameter and "int32_t" data type instead */
			LOCAL 6)	pagetable_level2_page_accesspermitions2		/* never use "L2_AP2" name */

			algorithm:	/* never use "x << 8" */
					return value << 010		/* return with "int32_t" data type */
			algorithm_end

			/* never use "#define" constant definition */
			/* never use "x" variable parameter, use "value" variable parameter and "int32_t" data type instead */
			LOCAL 7)	pagetable_level2_page_accesspermitions3		/* never use "L2_AP3" name */

			algorithm:	/* never use "x << 10" */
					return value << 012		/* return with "int32_t" data type */
			algorithm_end

			/* never use "#define" constant definition */
			/* never use "x" variable parameter, use "value" variable parameter and "int32_t" data type instead */
			LOCAL 8)	pagetable_level2_page_accesspermitions		/* never use "L2_AP" name */

			algorithm:	/* never use "L2_AP0(x) | L2_AP1(x) | L2_AP2(x) | L2_AP3(x)" */
					return (value << 012) | (value << 010) | (value << 006) | (value << 004)
					/* return with "int32_t" data type */
			algorithm_end

			/* never use "#define" constant definition */
			/* never use "x" variable parameter, use "value" variable parameter and "int32_t" data type instead */
			LOCAL 9)	pagetable_level2_page_armv7_small_typeextension	/* never use "L2_V7_S_TEX" name */

			algorithm:	/* never use "(x & 0x7) << 6" */
					return (007 & value) << 006	/* return with "int32_t" data type */
			algorithm_end

			/* never use "#define" constant definition */
			/* never use "x" variable parameter, use "value" variable parameter and "int32_t" data type instead */
			LOCAL 10)	pagetable_level2_page_armv7_accesspermitions	/* never use "L2_V7_AP" name */

			algorithm:	/* never use "((x & 0x4) << 7) | ((x & 0x2) << 4)" */
					return ((002 & value) << 004) | ((004 & value) << 007)
					/* return with "int32_t" data type */
			algorithm_end

	algorithm_end

// code_end
