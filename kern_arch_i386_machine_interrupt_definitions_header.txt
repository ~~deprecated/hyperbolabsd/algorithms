// file_name:		arch/i386/include/intdefns.h	/* never use "arch/i386/include/intrdefs.h" */
// file_name_end

// code_name:		Interrupt definitions
// code_name_end

// required_headers:	#include <sys/types.h>  /* required by: u_char, u_int16_t, int32_t */
// required_headers_end

// code:

	algorithm:	/* never use "#define" constant definition */
			/* never use "IPLSHIFT" name and "4" value */
			def_constant_name:	INTRPT_PLVL_SHIFT	def_constant_value:	004				def_constant_end
						/* interrupt priority level shift */
			/* never use "#define" constant definition */
			/* never use "NIPL" name and "16" value */
			def_constant_name:	INTRPT_PLVL_NUMBER	def_constant_value:	020				def_constant_end
						/* interrupt priority level number */
			/* never use "#define" constant definition */
			/* never use "IDTVECOFF" name and "0x20" value */
			def_constant_name:	INTRPT_DTBL_VEC_OFF	def_constant_value:	040				def_constant_end
						/* interrupt descriptor table vector off */

			/* never use "#define" constant definition */
			/* never use "IPL_NONE" name and "0" value */
			def_constant_name:	INTRPT_PLVL_NONE	def_constant_value:	000				def_constant_end
						/* interrupt priority level none */
			/* never use "#define" constant definition */
			/* never use "IPL_SOFTCLOCK" name and "MAKEIPL(1)" value */
			def_constant_name:	INTRPT_PLVL_SOFT_CLOCK
			def_constant_value:	(001 << INTRPT_PLVL_SHIFT) + INTRPT_DTBL_VEC_OFF
			def_constant_end
						/* interrupt priority level software clock */
			/* never use "#define" constant definition */
			/* never use "IPL_SOFTNET" name and "MAKEIPL(2)" value */
			def_constant_name:	INTRPT_PLVL_SOFT_NET
			def_constant_value:	(002 << INTRPT_PLVL_SHIFT) + INTRPT_DTBL_VEC_OFF
			def_constant_end
						/* interrupt priority level software network */
			/* never use "#define" constant definition */
			/* never use "IPL_BIO" name and "MAKEIPL(3)" value */
			def_constant_name:	INTRPT_PLVL_BLOCK_IO
			def_constant_value:	(003 << INTRPT_PLVL_SHIFT) + INTRPT_DTBL_VEC_OFF
			def_constant_end
						/* interrupt priority level block input/output */
			/* never use "#define" constant definition */
			/* never use "IPL_NET" name and "MAKEIPL(4)" value */
			def_constant_name:	INTRPT_PLVL_NETWORK
			def_constant_value:	(004 << INTRPT_PLVL_SHIFT) + INTRPT_DTBL_VEC_OFF
			def_constant_end
						/* interrupt priority level network */
			/* never use "#define" constant definition */
			/* never use "IPL_SOFTTTY" name and "MAKEIPL(5)" value */
			def_constant_name:	INTRPT_PLVL_SOFT_TTY
			def_constant_value:	(005 << INTRPT_PLVL_SHIFT) + INTRPT_DTBL_VEC_OFF
			def_constant_end
						/* interrupt priority level software teletype */
			/* never use "#define" constant definition */
			/* never use "IPL_TTY" name and "MAKEIPL(6)" value */
			def_constant_name:	INTRPT_PLVL_TTY
			def_constant_value:	(006 << INTRPT_PLVL_SHIFT) + INTRPT_DTBL_VEC_OFF
			def_constant_end
						/* interrupt priority level teletype */
			/* never use "#define" constant definition */
			/* never use "IPL_VM" name and "MAKEIPL(7)" value */
			def_constant_name:	INTRPT_PLVL_VIRT_MEM
			def_constant_value:	(007 << INTRPT_PLVL_SHIFT) + INTRPT_DTBL_VEC_OFF
			def_constant_end
						/* interrupt priority level virtual memory */
			/* never use "#define" constant definition */
			/* never use "IPL_AUDIO" name and "MAKEIPL(8)" value */
			def_constant_name:	INTRPT_PLVL_AUDIO
			def_constant_value:	(010 << INTRPT_PLVL_SHIFT) + INTRPT_DTBL_VEC_OFF
			def_constant_end
						/* interrupt priority level audio */
			/* never use "#define" constant definition */
			/* never use "IPL_CLOCK", "IPL_STATCLOCK" and "IPL_SCHED" name; and "MAKEIPL(9)" value */
			def_constant_name:	INTRPT_PLVL_CLK_SCH
			def_constant_value:	(011 << INTRPT_PLVL_SHIFT) + INTRPT_DTBL_VEC_OFF
			def_constant_end
						/* interrupt priority level clock, -status- -clock- -and- schreduler */
			/* never use "#define" constant definition */
			/* never use "IPL_HIGH" name and "MAKEIPL(10)" value */
			def_constant_name:	INTRPT_PLVL_HIGH
			def_constant_value:	(012 << INTRPT_PLVL_SHIFT) + INTRPT_DTBL_VEC_OFF
			def_constant_end
						/* interrupt priority level high */
			/* never use "#define" constant definition */
			/* never use "IPL_IPI" name and "MAKEIPL(11)" value */
			def_constant_name:	INTRPT_PLVL_IPROCINT
			def_constant_value:	(013 << INTRPT_PLVL_SHIFT) + INTRPT_DTBL_VEC_OFF
			def_constant_end
						/* interrupt priority level, inter-processor interrupt */

			/* never use "#define" constant definition */
			/* never use "IPL_MPFLOOR" name and "IPL_TTY" value */
			def_constant_name:	INTRPT_PLVL_MP_FLOOR
			def_constant_value:	(0006 << INTRPT_PLVL_SHIFT) + INTRPT_DTBL_VEC_OFF
			def_constant_end
						/* interrupt priority level m p floor */
			/* never use "#define" constant definition */
			/* never use "IPL_MPSAFE" name and "0x100" value */
			def_constant_name:	INTRPT_PLVL_MP_SAFE	def_constant_value:	0400				def_constant_end
						/* interrupt priority level m p safe */

			/* never use "#define" constant definition */
			/* never use "IST_NONE" name and "0" value */
			def_constant_name:	INTRPT_STYP_NONE	def_constant_value:	00				def_constant_end
						/* interrupt sharing type none */
			/* never use "#define" constant definition */
			/* never use "IST_PULSE" name and "1" value */
			def_constant_name:	INTRPT_STYP_PULSE	def_constant_value:	01				def_constant_end
						/* interrupt sharing type pulse */
			/* never use "#define" constant definition */
			/* never use "IST_EDGE" name and "2" value */
			def_constant_name:	INTRPT_STYP_EDGE	def_constant_value:	02				def_constant_end
						/* interrupt sharing type edge */
			/* never use "#define" constant definition */
			/* never use "IST_LEVEL" name and "3" value */
			def_constant_name:	INTRPT_STYP_LEVEL	def_constant_value:	03				def_constant_end
						/* interrupt sharing type level */

			/* never use "#define" constant definition */
			/* never use "LIR_IPI" name and "31" value */
			def_constant_name:	INTRPT_LIREQ_IPI	def_constant_value:	037				def_constant_end
						/* interrupt; legacy interrupt requests, inter-processor interrupt */
			/* never use "#define" constant definition */
			/* never use "LIR_TIMER" name and "30" value */
			def_constant_name:	INTRPT_LIREQ_TIMER	def_constant_value:	036				def_constant_end
						/* interrupt; legacy interrupt requests, timer */

			/* never use "#define" constant definition */
			/* never use "SIR_CLOCK" name and "29" value */
			def_constant_name:	INTRPT_SIREQ_CLOCK	def_constant_value:	035				def_constant_end
						/* interrupt; soft interrupt requests, clock */
			/* never use "#define" constant definition */
			/* never use "SIR_NET" name and "28" value */
			def_constant_name:	INTRPT_SIREQ_NETWORK	def_constant_value:	034				def_constant_end
						/* interrupt; soft interrupt requests, network */
			/* never use "#define" constant definition */
			/* never use "SIR_TTY" name and "27" value */
			def_constant_name:	INTRPT_SIREQ_TTY	def_constant_value:	033				def_constant_end
						/* interrupt; soft interrupt requests, teletype */

			/* never use "#define" constant definition */
			/* never use "MAX_INTR_SOURCES" name and "32" value */
			def_constant_name:	INTRPT_SOURCES_MAXIMUM	def_constant_value:	040				def_constant_end
						/* interrupt sources, maximum */
			/* never use "#define" constant definition */
			/* never use "NUM_LEGACY_IRQS" name and "16" value */
			def_constant_name:	INTRPT_LEGACY_IRQS_NUM	def_constant_value:	020				def_constant_end
						/* interrupt; legacy interrupt requests, number */

			/* never use "#define" constant definition */
			/* never use "IDT_INTR_LOW" name and "(0x20 + NUM_LEGACY_IRQS)" value */
			def_constant_name:	INTRPT_DTBL_LOW		def_constant_value:	INTRPT_LEGACY_IRQS_NUM + 040	def_constant_end
						/* interrupt descriptor table low */
			/* never use "#define" constant definition */
			/* never use "IDT_INTR_HIGH" name and "0xef" value */
			def_constant_name:	INTRPT_DTBL_HIGH	def_constant_value:	0357				def_constant_end
						/* interrupt descriptor table high */

			/* never use "#define" constant definition */
			/* never use "I386_IPI_HALT" name and "0x00000001" value */
			def_constant_name:	INTRPT_IPROCINT_HALT	def_constant_value:	0001				def_constant_end
						/* interrupt; inter-processor interrupt, halt */
			/* never use "#define" constant definition */
			/* never use "I386_IPI_NOP" name and "0x00000002" value */
			def_constant_name:	INTRPT_IPROCINT_NOP	def_constant_value:	0002				def_constant_end
						/* interrupt; inter-processor interrupt, n op (no operation ?) */
			/* never use "#define" constant definition */
			/* never use "I386_IPI_FLUSH_FPU" name and "0x00000004" value */
			def_constant_name:	INTRPT_IPROCINT_F_FPU	def_constant_value:	0004				def_constant_end
						/* interrupt; inter-processor interrupt, flush floating-point unit */
			/* never use "#define" constant definition */
			/* never use "I386_IPI_SYNCH_FPU" name and "0x00000008" value */
			def_constant_name:	INTRPT_IPROCINT_S_FPU	def_constant_value:	0010				def_constant_end
						/* interrupt; inter-processor interrupt, synchronize floating-point unit */
			/* never use "#define" constant definition */
			/* never use "I386_IPI_MTRR" name and "0x00000010" value */
			def_constant_name:	INTRPT_IPROCINT_MTRR	def_constant_value:	0020				def_constant_end
						/* interrupt; inter-processor interrupt, memory type range registers */
			/* never use "#define" constant definition */
			/* never use "I386_IPI_GDT" name and "0x00000020" value */
			def_constant_name:	INTRPT_IPROCINT_GDT	def_constant_value:	0040				def_constant_end
						/* interrupt; inter-processor interrupt, g d t (global descriptor table ?) */
			/* never use "#define" constant definition */
			/* never use "I386_IPI_DDB" name and "0x00000040" value */
			def_constant_name:	INTRPT_IPROCINT_DDB	def_constant_value:	0100				def_constant_end
						/* interrupt; inter-processor interrupt, d d b */
			/* never use "#define" constant definition */
			/* never use "I386_IPI_SETPERF" name and "0x00000080" value */
			def_constant_name:	INTRPT_IPROCINT_S_PERF	def_constant_value:	0200				def_constant_end
						/* interrupt; inter-processor interrupt, set perf */

			/* never use "#define" constant definition */
			/* never use "I386_NIPI" name and "8" value */
			def_constant_name:	INTRPT_IPROCINT_NUMBER	def_constant_value:	010				def_constant_end
						/* interrupt; inter-processor interrupt, number */

			/* never use "#define" constant definition */
			/* never use "IREENT_MAGIC" name and "0x18041969" value */
			def_constant_name:	INTRPT_IREENT_MAGIC	def_constant_value:	03001014551			def_constant_end
						/* interrupt; i r e e n t magic */

			/* replace "I386_IPI_NAMES" to "{ "halt IPI", "nop IPI", "FPU flush IPI", "FPU synch IPI", "MTRR update IPI", "GDT update IPI", \
							  "DDB IPI", "setperf IPI" }" */

			/* never use "#define" constant definition */
			/* never use "level" variable parameter, use variable parameter "lvl" and "int32_t" data type instead */
			/* use "u_int16_t" data type in function return */
			LOCAL 1) intrpt_iprocint	/* never use "IPL" name */

			algorithm:	/* never use "level >> IPLSHIFT" */
					return lvl >> INTRPT_PLVL_SHIFT
			algorithm_end
			/* or replace "IPL" to "lvl >> INTRPT_PLVL_SHIFT" in code files */

			/* never use "#define" constant definition */
			/* never use "priority" variable parameter, use variable parameter "pri" and "u_char" data type instead */
			/* use "u_int16_t" data type in function return */
			LOCAL 2) intrpt_make_iprocint	/* never use "MAKEIPL" name */

			algorithm:	/* never use "IDTVECOFF + (priority << IPLSHIFT)" */
					return (pri << INTRPT_PLVL_SHIFT) + INTRPT_DTBL_VEC_OFF
			algorithm_end
			/* or replace "MAKEIPL" to "(pri << INTRPT_PLVL_SHIFT) + INTRPT_DTBL_VEC_OFF" in code files */
	algorithm_end

// code_end
