// file_name:		arch/arm/include/prom.h	/* never use "arch/arm/include/openpromio.h" */
// file_name_end

// code_name:		Programable Read Only Memory (PROM)
// code_name_end

// required_headers:	#include <sys/types.h>	/* required by: int8_t, int32_t */
//			#include <sys/ioccom.h>	/* required by: _IOC, IOC_INOUT, IOC_IN, IOC_OUT */
// required_headers_end

// code:

	algorithm:	/* never use "opiocdesc" tag */
			struct_tag:	prom_ioc_desc_t		/* "prom_ioc_description_t" */
			struct_name:
			struct_start:
				struct_tag:
				struct_name:	node_s
				struct_start:
					/* never use "op_nodeid" name */
					/* never use "int" data type */
					data_declaration_type:	int32_t
					data_declaration_name:	id	/* "identification" */
					data_declaration_end
				struct_end

				struct_tag:
				struct_name:	name_s
				struct_start:
					/* never use "op_namelen" name */
					/* never use "int" data type */
					data_declaration_type:	int32_t
					data_declaration_name:	length
					data_declaration_end

					/* never use "op_name" name */
					/* never use "char" data type */
					/* it is a pointer */
					data_declaration_type:	int8_t *
					data_declaration_name:	pointer
					data_declaration_end
				struct_end

				struct_tag:
				struct_name:	buffer_s
				struct_start:
					/* never use "op_buflen" name */
					/* never use "int" data type */
					data_declaration_type:	int32_t
					data_declaration_name:	length
					data_declaration_end

					/* never use "op_buf" name */
					/* never use "char" data type */
					/* it is a pointer */
					data_declaration_type:	int8_t *
					data_declaration_name:	pointer
					data_declaration_end
				struct_end
			struct_end

			/* never use "#define" constant definition */
			/* never use "OPIOCGET" name and "_IOWR('O', 1, struct opiocdesc)" value */
			def_constant_name:	PROM_IO_GET
			def_constant_value:	_IOC( IOC_INOUT, 0117, 01, sizeof (struct prom_ioc_desc_t) )
			def_constant_end
						/* programable read only memory input output get */
			/* never use "#define" constant definition */
			/* never use "OPIOCSET" name and "_IOW('O', 2, struct opiocdesc)" value */
			def_constant_name:	PROM_IO_SET
			def_constant_value:	_IOC( IOC_IN, 0117, 02, sizeof (struct prom_ioc_desc_t) )
			def_constant_end
						/* programable read only memory input output set */
			/* never use "#define" constant definition */
			/* never use "OPIOCNEXTPROP" name and "_IOWR('O', 3, struct opiocdesc)" value */
			def_constant_name:	PROM_IO_NEXT_PROPERTY
			def_constant_value:	_IOC( IOC_INOUT, 0117, 03, sizeof (struct prom_ioc_desc_t) )
			def_constant_end
						/* programable read only memory input output next property */
			/* never use "#define" constant definition */
			/* never use "OPIOCGETOPTNODE" name and "_IOR('O', 4, int)" value */
			def_constant_name:	PROM_IO_GET_OPT_NODE
			def_constant_value:	_IOC( IOC_OUT, 0117, 04, sizeof (int32_t) )
			def_constant_end
						/* programable read only memory input output get option node */
			/* never use "#define" constant definition */
			/* never use "OPIOCGETNEXT" name and "_IOWR('O', 5, int)" value */
			def_constant_name:	PROM_IO_GET_NEXT_NODE
			def_constant_value:	_IOC( IOC_INOUT, 0117, 05, sizeof (int32_t) )
			def_constant_end
						/* programable read only memory input output get next node */
			/* never use "#define" constant definition */
			/* never use "OPIOCGETCHILD" name and "_IOWR('O', 6, int)" value */
			def_constant_name:	PROM_IO_GET_CHILD
			def_constant_value:	_IOC( IOC_INOUT, 0117, 06, sizeof (int32_t) )
			def_constant_end
						/* programable read only memory input output get child */
	algorithm_end

// code_end
